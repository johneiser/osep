# OSEP

Tools and documentation related to the participation in Offensive Security PEN-300 (OSEP), including modules for use in the [lets](https://github.com/johneiser/lets) framework.

## Requirements

- [docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
- python >= 3.5
- python3-pip
- [lets](https://lets.readthedocs.io/en/latest/install.html)

## Install

**lets** is built on top of [docker](https://docs.docker.com/install/linux/docker-ce/ubuntu), so make sure it is installed. You may need to log out and back in for this to take effect.

```
$ curl -fsSL https://get.docker.com | sudo sh
$ sudo usermod -aG docker $USER
```

Use a python virtual environment
```
$ mkdir /tmp/venv
$ python -m virtualenv -p python3 /tmp/venv/osep
$ source /tmp/venv/osep/bin/activate
(osep) $
```

Install **lets_osep**

```
(osep) $ pip3 install git+https://bitbucket.org/johneiser/osep.git
```

Activate **lets** *tab-completion* for bash.

```
(osep) $ source <(lets support/autocomplete bash)
(osep) $ lets sample/my[TAB][TAB]
sample/mydockermodule   sample/mymodule
```

## Usage

Quickstart:

```
$ echo SGVsbG8gd29ybGQhCg== | lets decode/base64
Hello world!
```

For further details, refer to the [docs](https://lets.readthedocs.io/en/latest/usage.html).
