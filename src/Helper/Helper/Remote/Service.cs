﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Helper.Remote
{
    public class Service
    {
        public static string ServiceName = "SensorDataService";

        private struct QueryServiceConfigStruct
        {
            public int serviceType;
            public int startType;
            public int errorControl;
            public IntPtr binaryPathName;
            public IntPtr loadOrderGroup;
            public int tagID;
            public IntPtr dependencies;
            public IntPtr startName;
            public IntPtr displayName;
        }

        [DllImport("advapi32.dll", EntryPoint = "OpenSCManagerW", ExactSpelling = true, CharSet = CharSet.Unicode, SetLastError = true)]
        private static extern IntPtr OpenSCManager(string machineName, string databaseName, uint dwAccess);

        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern IntPtr OpenService(IntPtr hSCManager, string lpServicename, uint dwDesiredAccess);

        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern int QueryServiceConfig(IntPtr service, IntPtr queryServiceConfig, int bufferSize, ref int bytesNeeded);

        [DllImport("advapi32.dll", EntryPoint = "ChangeServiceConfig")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool ChangeServiceConfigA(IntPtr hService, uint dwServiceType, int dwStartType, int dwErrorControl, string lpBinaryPathName, string lpLoadOrderGroup, string lpdwTagId, string lpDependencies, string lpServiceStartName, string lpPassword, string lpDisplayName);

        [DllImport("advapi32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool StartService(IntPtr hService, int dwNumServiceArgs, string[] lpServiceArgVectors);

        [DllImport("kernel32.dll")]
        private static extern uint GetLastError();

        [DllImport("advapi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool CloseServiceHandle(IntPtr hSCObject);

        private static void _Execute(IntPtr schService, string command, bool restore = true)
        {
            string original = null;

            if (restore)
            {
                // Perform empty query to discover bytes needed for service configuration
                int pcbBytesNeeded = 5;
                IntPtr qscPtr = Marshal.AllocCoTaskMem(0);
                if (QueryServiceConfig(schService, qscPtr, 0, ref pcbBytesNeeded) == 0 && pcbBytesNeeded == 0)
                    throw new System.ComponentModel.Win32Exception("Failed to read configuration for " + ServiceName);

                // Query service configuration
                qscPtr = Marshal.AllocCoTaskMem(pcbBytesNeeded);
                if (QueryServiceConfig(schService, qscPtr, pcbBytesNeeded, ref pcbBytesNeeded) == 0)
                    throw new System.ComponentModel.Win32Exception("Failed to read configuration for " + ServiceName);

                // Parse service configuration
                QueryServiceConfigStruct qscs = new QueryServiceConfigStruct();
                qscs.binaryPathName = IntPtr.Zero;
                qscs = (QueryServiceConfigStruct)Marshal.PtrToStructure(qscPtr, new QueryServiceConfigStruct().GetType());
                original = Marshal.PtrToStringAuto(qscs.binaryPathName);
                Marshal.FreeCoTaskMem(qscPtr);
            }

            // Modify service configuration
            if (!ChangeServiceConfigA(schService, 0xFFFFFFFF, 3, 0, command, null, null, null, null, null, null)) // SERVICE_NO_CHANGE
                throw new System.ComponentModel.Win32Exception("Failed to change service binary to " + command);

            // Start service
            if (!StartService(schService, 0, null) && GetLastError() != 1053)
                throw new System.ComponentModel.Win32Exception("Failed to start " + ServiceName);

            if (restore && original != null)
            {
                // Restore service configuration
                if (!ChangeServiceConfigA(schService, 0xFFFFFFFF, 3, 0, original, null, null, null, null, null, null))
                    throw new System.ComponentModel.Win32Exception("Failed to restore service binary to " + original);
            }
        }

        private static void _OpenAndExecute(IntPtr SCMHandle, string command)
        {
            IntPtr schService = OpenService(SCMHandle, ServiceName, 0xF01FF);   // SERVICE_ALL_ACCESS
            if (schService == IntPtr.Zero)
                throw new System.ComponentModel.Win32Exception("Failed to open " + ServiceName);

            try
            {
                _Execute(schService, command);
            }
            finally
            {
                CloseServiceHandle(schService);
            }
        }

        private static void _AccessOpenAndExecute(string target, string command = null)
        {
            IntPtr SCMHandle = OpenSCManager(target, null, 0xF003F);    // SC_MANAGER_ALL_ACCESS
            if (SCMHandle == IntPtr.Zero)
                throw new System.ComponentModel.Win32Exception("Failed to connect to " + target);

            try
            {
                if (command != null)
                    _OpenAndExecute(SCMHandle, command);
            }
            finally
            {
                CloseServiceHandle(SCMHandle);
            }
        }

        private static string _Check(string target)
        {
            try
            {
                _AccessOpenAndExecute(target);
                return "Access Granted: " + target;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string Check(params string[] targets)
        {
            StringBuilder results = new StringBuilder();

            foreach (string target in targets)
                results.AppendLine(_Check(target));

            return results.ToString();
        }

        public static string Execute(string target, string command)
        {
            try
            {
                _AccessOpenAndExecute(target, command);
                return "Execution Complete: " + target;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}
