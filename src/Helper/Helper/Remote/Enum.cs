﻿using System;
using System.Text;

namespace Helper.Remote
{
    public class Enum
    {
        public static string GetDomain()
        {
            try
            {
                return Common.PShell("Get-Domain");
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string GetForest()
        {
            try
            {
                return Common.PShell("Get-Forest");
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string GetForestDomains(string forest = null)
        {
            try
            {
                StringBuilder cmd = new StringBuilder();
                cmd.Append("Get-ForestDomain");
                if (forest != null)
                    cmd.Append(" -Forest '" + forest + "'");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string GetTrustedDomains()
        {
            try
            {
                return Common.Shell("nltest /trusted_domains");
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string GetDomainTrust(string domain = null)
        {
            try
            {
                StringBuilder cmd = new StringBuilder();
                cmd.Append("Get-DomainTrust");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string GetDomainTrustMapping()
        {
            try
            {
                return Common.PShell("Get-DomainTrustMapping");
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string GetDomainSID(string domain = null)
        {
            try
            {
                StringBuilder cmd = new StringBuilder();
                cmd.Append("Get-DomainSID");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string GetGroups(string domain = null)
        {
            try
            {
                StringBuilder cmd = new StringBuilder();
                cmd.Append("Get-DomainGroup");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                cmd.Append(" | ForEach-Object {");
                cmd.AppendLine(" New-Object PSObject -Property @{");
                cmd.AppendLine("name = $_.distinguishedname");
                cmd.AppendLine("memberof = ($_.memberof -Join \",\")");
                cmd.AppendLine("description = $_.description");
                if (domain != null)
                    cmd.AppendLine("members = (( Get-DomainGroupMember -Domain '" + domain + "' -Identity $_.samaccountname | ForEach-Object { ConvertFrom-SID $_.MemberSID }) -Join \",\")");
                else
                    cmd.AppendLine("members = (( Get-DomainGroupMember -Identity $_.samaccountname | ForEach-Object { ConvertFrom-SID $_.MemberSID }) -Join \",\")");
                cmd.Append("}");
                cmd.Append("}");
                cmd.Append(" | Where-Object -Property members");
                cmd.Append(" | Select-Object -Property name,members");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string GetGroupMembership(string domain = null)
        {
            try
            {
                StringBuilder cmd = new StringBuilder();
                cmd.Append("Get-DomainGroup");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                cmd.Append(" | Where-Object -Property memberof");
                cmd.Append(" | ForEach-Object {");
                cmd.AppendLine(" New-Object PSObject -Property @{");
                cmd.AppendLine("name = $_.distinguishedname");
                cmd.AppendLine("memberof = ($_.memberof -Join \",\")");
                cmd.AppendLine("description = $_.description");
                if (domain != null)
                    cmd.AppendLine("members = ((Get-DomainGroupMember -Domain '" + domain + "' -Identity $_.samaccountname | ForEach-Object { ConvertFrom-SID $_.MemberSID }) -Join \",\")");
                else
                    cmd.AppendLine("members = ((Get-DomainGroupMember -Identity $_.samaccountname | ForEach-Object { ConvertFrom-SID $_.MemberSID }) -Join \",\")");
                cmd.Append("}");
                cmd.Append("}");
                cmd.Append(" | Select-Object -Property name,memberof,members");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string GetGroupMembers(string group, string domain = null)
        {
            try
            {
                StringBuilder cmd = new StringBuilder();
                cmd.Append("Get-DomainGroupMember");
                cmd.Append(" -Identity '" + group + "'");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string GetForeignUsers(string domain = null)
        {
            try
            {
                StringBuilder cmd = new StringBuilder();
                cmd.Append("Get-DomainForeignUser");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string GetForeignGroupMembers(string domain = null)
        {
            try
            {
                StringBuilder cmd = new StringBuilder();
                cmd.Append("Get-DomainForeignGroupMember");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                cmd.Append(" | ForEach-Object {");

                cmd.Append(" $_ | Add-Member -NotePropertyName Identity -NotePropertyValue (");
                cmd.Append(" ConvertFrom-SID");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                cmd.Append(" $_.MemberName");
                cmd.Append(" ) -Force; $_");

                cmd.Append("}");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string Resolve(string computer = null)
        {
            try
            {
                if (computer == null)
                    computer = Environment.GetEnvironmentVariable("USERDOMAIN");

                StringBuilder cmd = new StringBuilder();
                cmd.Append("Get-NetComputerSiteName");
                if (computer != null)
                    cmd.Append(" -ComputerName '" + computer + "'");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }


        public static string GetShares(string computer = null)
        {
            try
            {
                StringBuilder cmd = new StringBuilder();
                cmd.Append("Get-NetShare");
                if (computer != null)
                    cmd.Append(" -ComputerName '" + computer + "'");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string GetLoggedOn(string computer = null)
        {
            try
            {
                StringBuilder cmd = new StringBuilder();
                cmd.Append("Get-NetLoggedon");
                if (computer != null)
                    cmd.Append(" -ComputerName '" + computer + "'");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string GetLoggedOnRDP(string computer = null)
        {
            try
            {
                StringBuilder cmd = new StringBuilder();
                cmd.Append("Get-NetRDPSession");
                if (computer != null)
                    cmd.Append(" -ComputerName '" + computer + "'");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string GetProxy(string computer = null)
        {
            try
            {
                StringBuilder cmd = new StringBuilder();
                cmd.Append("Get-WMIRegProxy");
                if (computer != null)
                    cmd.Append(" -ComputerName '" + computer + "'");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        // TODO: Not accurate, please fix
        /*public static string GetServices(string domain = null)
        {
            try
            {
                StringBuilder cmd = new StringBuilder();
                cmd.Append("Get-DomainUser");
                cmd.Append(" -SPN");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                cmd.Append(" | Select-Object -Property distinguishedname");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }*/

        public static string GetLocalGroupMembers(string computer = null)
        {
            try
            {
                StringBuilder cmd = new StringBuilder();
                cmd.Append("Get-NetLocalGroupMember");
                cmd.Append(" -Method API");
                if (computer != null)
                    cmd.Append(" -ComputerName '" + computer + "'");
                cmd.Append(" | Select-Object -Property groupname,membername");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        /*Get-DomainUser -Domain $Domain | ForEach-Object {
            Get-ObjectAcl -Domain $Domain -Identity $_.name | ForEach-Object {
                $_ | Add-Member -NotePropertyName Identity -NotePropertyValue (
                    ConvertFrom-SID -Domain $Domain $_.SecurityIdentifier
                ) -Force
                if ($_.Identity -match $Identity) {$_}
            }
        }*/
        public static string GetUserRights(string identity = null, string domain = null)
        {
            try
            {
                if (identity == null)
                    identity = Environment.GetEnvironmentVariable("Username");

                StringBuilder cmd = new StringBuilder();
                cmd.Append("Get-DomainUser");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                cmd.Append(" | ForEach-Object {");

                cmd.Append("Get-ObjectAcl");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                cmd.Append(" -Identity $_.name");
                cmd.Append(" | ForEach-Object {");

                cmd.Append(" $_ | Add-Member -NotePropertyName Identity -NotePropertyValue (");
                cmd.Append(" ConvertFrom-SID");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                cmd.Append(" $_.SecurityIdentifier");
                cmd.Append(" ) -Force; If ($_.Identity -match '" + identity + "') { $_ }");

                cmd.Append("}");
                cmd.Append("}");

                cmd.Append(" | Select-Object -Property Identity, ObjectDN, ActiveDirectoryRights");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }


        /*Get-DomainGroup -Domain $Domain | ForEach-Object {
            Get-ObjectAcl -Domain $Domain -Identity $_.name | ForEach-Object {
                $_ | Add-Member -NotePropertyName Identity -NotePropertyValue (
                    ConvertFrom-SID -Domain $Domain $_.SecurityIdentifier
                ) -Force
                if ($_.Identity -match $Identity) {$_}
            }
        } */
        public static string GetGroupRights(string identity = null, string domain = null)
        {
            try
            {
                if (identity == null)
                    identity = Environment.GetEnvironmentVariable("Username");

                StringBuilder cmd = new StringBuilder();
                cmd.Append("Get-DomainGroup");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                cmd.Append(" | ForEach-Object {");

                cmd.Append("Get-ObjectAcl");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                cmd.Append(" -Identity $_.name");
                cmd.Append(" | ForEach-Object {");

                cmd.Append(" $_ | Add-Member -NotePropertyName Identity -NotePropertyValue (");
                cmd.Append(" ConvertFrom-SID");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                cmd.Append(" $_.SecurityIdentifier");
                cmd.Append(" ) -Force; If ($_.Identity -match '" + identity + "') { $_ }");

                cmd.Append("}");
                cmd.Append("}");

                cmd.Append(" | Select-Object -Property Identity, ObjectDN, ActiveDirectoryRights");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        /*Get-DomainComputer -Domain $Domain | ForEach-Object {
            Get-ObjectAcl -Domain $Domain -Identity $_.name | ForEach-Object {
                $_ | Add-Member -NotePropertyName Identity -NotePropertyValue (
                    ConvertFrom-SID -Domain $Domain $_.SecurityIdentifier
                ) -Force
                if ($_.Identity -match $Identity) {$_}
            }
        } */
        public static string GetComputerRights(string identity = null, string domain = null)
        {
            try
            {
                if (identity == null)
                    identity = Environment.GetEnvironmentVariable("Username");

                StringBuilder cmd = new StringBuilder();
                cmd.Append("Get-DomainComputer");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                cmd.Append(" | ForEach-Object {");

                cmd.Append("Get-ObjectAcl");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                cmd.Append(" -Identity $_.name");
                cmd.Append(" | ForEach-Object {");

                cmd.Append(" $_ | Add-Member -NotePropertyName Identity -NotePropertyValue (");
                cmd.Append(" ConvertFrom-SID");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                cmd.Append(" $_.SecurityIdentifier");
                cmd.Append(" ) -Force; If ($_.Identity -match '" + identity + "') { $_ }");

                cmd.Append("}");
                cmd.Append("}");

                cmd.Append(" | Select-Object -Property Identity, ObjectDN, ActiveDirectoryRights");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string GetRights(string identity = null, string domain = null)
        {
            StringBuilder results = new StringBuilder();
            try
            {
                results.AppendLine("User:");
                results.AppendLine(GetUserRights(identity, domain));
                results.AppendLine("Group:");
                results.AppendLine(GetGroupRights(identity, domain));
                results.AppendLine("Computer:");
                results.AppendLine(GetComputerRights(identity, domain));
            }
            catch (Exception e)
            {
                return results.ToString() + e.Message;
            }
            return results.ToString();
        }

        public static string GetUnconstrainedDelegation(string domain = null)
        {
            try
            {
                StringBuilder cmd = new StringBuilder();
                cmd.Append("Get-DomainComputer");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                cmd.Append(" -Unconstrained");
                cmd.Append(" | Select-Object");
                cmd.Append(" -Property name, useraccountcontrol");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        /*
         Get-DomainUser -Domain $Domain -TrustedToAuth | ForEach-Object {
                New-Object PSObject -Property @{
                    samaccountname = $_.samaccountname
                    "msds-allowedtodelegateto" = ($_."msds-allowedtodelegateto" -Join ",")
                    useraccountcontrol = $_.useraccountcontrol
                }
            }
         */
        public static string GetUserConstrainedDelegation(string domain = null)
        {
            try
            {
                StringBuilder cmd = new StringBuilder();
                cmd.Append("Get-DomainUser");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                cmd.Append(" -TrustedToAuth");
                cmd.Append(" | ForEach-Object {");
                cmd.AppendLine(" New-Object PSObject -Property @{");
                cmd.AppendLine("samaccountname = $_.samaccountname");
                cmd.AppendLine("\"msds-allowedtodelegateto\" = ($_.\"msds-allowedtodelegateto\" -Join \",\")");
                cmd.AppendLine("useraccountcontrol = $_.useraccountcontrol");
                cmd.Append("}");
                cmd.Append("}");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        /*
         Get-DomainComputer -Domain $Domain -TrustedToAuth | ForEach-Object {
                New-Object PSObject -Property @{
                    samaccountname = $_.samaccountname
                    "msds-allowedtodelegateto" = ($_."msds-allowedtodelegateto" -Join ",")
                    useraccountcontrol = $_.useraccountcontrol
                }
            }
         */
        public static string GetComputerConstrainedDelegation(string domain = null)
        {
            try
            {
                StringBuilder cmd = new StringBuilder();
                cmd.Append("Get-DomainComputer");
                if (domain != null)
                    cmd.Append(" -Domain '" + domain + "'");
                cmd.Append(" -TrustedToAuth");
                cmd.Append(" | ForEach-Object {");
                cmd.AppendLine(" New-Object PSObject -Property @{");
                cmd.AppendLine("samaccountname = $_.samaccountname");
                cmd.AppendLine("\"msds-allowedtodelegateto\" = ($_.\"msds-allowedtodelegateto\" -Join \",\")");
                cmd.AppendLine("useraccountcontrol = $_.useraccountcontrol");
                cmd.Append("}");
                cmd.Append("}");
                return Common.PShell(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string GetConstrainedDelegation(string domain = null)
        {
            StringBuilder results = new StringBuilder();
            try
            {
                results.AppendLine("User:");
                results.AppendLine(GetUserConstrainedDelegation(domain));
                results.AppendLine("Computer:");
                results.AppendLine(GetComputerConstrainedDelegation(domain));
            }
            catch (Exception e)
            {
                return results.ToString() + e.Message;
            }
            return results.ToString();
        }

        public static string GetDelegation(string domain = null)
        {
            StringBuilder results = new StringBuilder();
            try
            {
                results.AppendLine("Unconstrained:");
                results.AppendLine(GetUnconstrainedDelegation(domain));
                results.AppendLine("Constrained User:");
                results.AppendLine(GetUserConstrainedDelegation(domain));
                results.AppendLine("Constrained Computer:");
                results.AppendLine(GetComputerConstrainedDelegation(domain));
            }
            catch (Exception e)
            {
                return results.ToString() + e.Message;
            }
            return results.ToString();
        }

        // Fail, run directly with powershell_import /tmp/LAPSToolkit.ps1 (needs powershell 2?)
        /*public static string GetLAPSComputers()
        {
            try
            {
                return Common.PShell("Get-LAPSComputers | Select");
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
*/
        // Fail, run directly with powershell_import /tmp/LAPSToolkit.ps1 (needs powershell 2?)
        /*public static string GetLAPSReaders()
        {
            try
            {
                return Common.PShell("Find-LAPSDelegatedGroups | ForEach-Object { Get-NetGroupMember -GroupName $_.DelegatedGroups | Select }");
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
*/
    }
}
