﻿

Function Helper-Enable-ConstrainedDelegation
{
    [CmdletBinding()]
    Param (
        [Parameter(Position = 0)]
        [String]
        $Target,

        [Parameter(Position = 1)]
        [String]
        $Identity,

        [Parameter(Position = 2)]
        [String]
        $Domain = $null
    )
    Process {
        try {
            $sid = Get-DomainComputer -Domain $Domain -Identity $Identity -Properties objectsid | Select -Expand objectsid
            $SD = New-Object Security.AccessControl.RawSecurityDescriptor -ArgumentList "O:BAD:(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;$($sid))"
            $SDBytes = New-Object byte[] $SD.BinaryLength
            $SD.GetBinaryForm($SDBytes, 0)
            Set-DomainObject -Domain $Domain -Identity $Target -Set @{
                "msds-allowedtoactonbehalfofotheridentity"=$SDBytes
            } -Verbose | Select-Object
        } catch [System.Management.Automation.CommandNotFoundException] {
            "Missing an import: " + $_
        }
    }
}

Function Helper-Check-ConstrainedDelegation
{
    [CmdletBinding()]
    Param (
        [Parameter(Position = 0)]
        [String]
        $Target,

        [Parameter(Position = 1)]
        [String]
        $Identity,

        [Parameter(Position = 2)]
        [String]
        $Domain = $null
    )
    Process {
        try {
            New-Object Security.AccessControl.RawSecurityDescriptor -ArgumentList $(
                Get-DomainComputer -Domain $Domain -Identity $Target -Properties 'msds-allowedtoactonbehalfofotheridentity' | select -expand msds-allowedtoactonbehalfofotheridentity
            ).DiscretionaryAcl | ForEach-Object {
                $_ | Add-Member -NotePropertyName Identity -NotePropertyValue (
                    ConvertFrom-SID -Domain $Domain $_.SecurityIdentifier
                ) -Force
            }
        } catch [System.Management.Automation.CommandNotFoundException] {
            "Missing an import: " + $_
        }
    }
}