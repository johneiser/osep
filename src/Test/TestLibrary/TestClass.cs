﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

[ComVisible(true)]
public class TestClass
{
    public TestClass()
    {
        MessageBox.Show("Init", "TestClass", MessageBoxButtons.OK, MessageBoxIcon.Information);
    }

    public void Test(string message)
    {
        MessageBox.Show(message, "TestClass", MessageBoxButtons.OK, MessageBoxIcon.Information);
    }
}

