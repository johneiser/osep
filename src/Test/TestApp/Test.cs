﻿using System;
using System.Windows.Forms;

public class Test
{
    public static void Main(string[] args)
    {
        MessageBox.Show("Main", "TestApp", MessageBoxButtons.OK, MessageBoxIcon.Information);
        
        foreach (string arg in args)
            MessageBox.Show(arg, "TestApp", MessageBoxButtons.OK, MessageBoxIcon.Information);
    }

    public static void Do()
    {
        MessageBox.Show("Do", "TestApp", MessageBoxButtons.OK, MessageBoxIcon.Information);
    }
}