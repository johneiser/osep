
Changelog
=========

2.0.0
^^^^^

March 22, 2021 - Exam Attempt!

- Added Helper, along with __data__/helper.dll(.gz)
- Added various __data__/tools/{powershell,csharp,native,python}
- Added format/cmd/vba
- Added __images__/local/responder
- Added listen/{capture,relay}/ntlm
- Added format/windows/assembly/csharp_aspx
- Added format/csharp/aspx


1.1.1
^^^^^

February 7, 2021

- Added new-process migration to Loader


1.1.0
^^^^^

February 6, 2021

- Modified Loader to load and use a private copy of ntdll.dll where possible
- Removed AMSI bypass from Loader
- Added __data__/template.lnk and format/cmd/lnk
- Added __data__/dnscat2-v0.07-client-win32.exe


1.0.16
^^^^^^

February 4, 2021

- Added format/windows/assembly/jscript
- Added generate/payload/jscript/{web,dns}loader


1.0.15
^^^^^^

February 3, 2021

- Added format/windows/assembly/msbuild


1.0.14
^^^^^^

February 2, 2021

- Added format/windows/assembly/csharp_{installutil,workflowcompiler}
- Added generate/payload/xml/workflowcompiler


1.0.13
^^^^^^

February 1, 2021

- Removed 'wait' from Loader function definitions
- Added format/windows/assembly/csharp


1.0.12
^^^^^^

February 1, 2021

- Fixed certificate pinning in Runner
- Fixed proxy-awareness in Runner


1.0.11
^^^^^^

January 31, 2021

- Added __data__/{loader32,loader64,runner,test}.dll.gz


1.0.10
^^^^^^

January 31, 2021

- Added encode/powershell/bypass/dynamic


1.0.9
^^^^^

January 11, 2021

- Added __data__/loader{32,64}.dll.delegate (dotnet2jscript)
- Added __data__/loader{32,64}.dll (raw)
- Added __data__/runner.dll (raw)


1.0.8
^^^^^

January 11, 2021

- Moved csharp solutions to /src
- Added __data__/{runner,loader}.dll.delegate (dotnet2jscript)


1.0.7
^^^^^

January 11, 2021

- Added AMSI bypass (patch) to Runner and Loader
- Added process injection to Loader
- Changed Loader to x64-only release


1.0.6
^^^^^

January 10, 2021

- Added __data__/{Test,Runner,Loader} csharp solutions
- Added format/windows/x64/pic/csharp (from SharpShooter)
- Added dependency on pycryptodomex


1.0.5
^^^^^

January 3, 2021

- Moved encode/jscript/bypass/amsi to encode/jscript/bypass/amsi/registry
- Added encode/jscript/bypass/amsi/dll


1.0.4
^^^^^

January 3, 2021

- Slight modification to connect/lab
- Added dependency on docker-lets-pentest
- Added JScript registry AMSI bypass


1.0.3
^^^^^

December 20, 2020

- Moved encode/js/obfuscate to lets_pentest


1.0.2
^^^^^

December 19, 2020

- Added encode/js/obfuscate


1.0.1
^^^^^

December 19, 2020

- Added connect/lab


1.0.0
^^^^^

December 19, 2020

- Initial upload
