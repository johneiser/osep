from lets.__module__ import Module, Mount, Container


class NTLM(Module):
    """
    Start an SMB server to listen for NetNTLM hash authentication.
    """
    images = [
        "local/responder:1.0.0",
        ]

    @classmethod
    def add_arguments(cls, parser):
        parser.add_argument("-n", "--network-interface", type=str, required=True,
            help="interface to bind smb server (e.g. tun0)")
        parser.add_argument("-p", "--port", type=int, default=445,
            help="port to bind smb server (%(default)i)")
        parser.add_argument("-w", "--wpad", action="store_true",
            help="start a rogue wpad server")

    def handle(self, input, network_interface=None, port=445, wpad=False):

        with Mount("/data") as mount:

            # Construct command
            cmd  = "responder"
            cmd += " -I %s" % network_interface
            if wpad:
                cmd += " -w"

            # Launch responder
            with Container.run("local/responder:1.0.0",
                stdin_open=True,
                tty=True,
                network="host",
                volumes=mount.volumes,
                command=cmd) as container:

                container.interact()
