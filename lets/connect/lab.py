from lets.__module__ import Module, Container
import argparse


class Lab(Module):
    """
    Connect to the OSEP lab network over VPN.
    """
    images = [
        "dperson/openvpn-client:latest",    # Cross-platform, no versions
        ]

    @classmethod
    def add_arguments(self, parser):
        parser.add_argument("-p", "--path", type=str, default="/data/vpn/osep",
            help="Path to OSEP vpn.conf and vpn.auth (user\npass)")
        parser.add_argument("-f", "--full", action="store_true",
            help="Direct all traffic over VPN connection")
        parser.add_argument("-n", "--name", type=str, default="osep_net",
            help="Name of container with tunnel network")

    def handle(self, input, path="/data/vpn/osep", full=False, name="osep_net"):

        # Launch tunnel
        self.log.info("Connecting to lab...")
        with Container.run("dperson/openvpn-client:latest",
            name=name,
            cap_add=["NET_ADMIN"],
            devices=["/dev/net/tun"],
            network="host" if full else None,
            volumes={
                "/data/vpn/osep" : {
                    "bind" : "/vpn",
                    "mode" : "ro",
                }
            }) as vpn:

                # Wait for successful connection
                for log in vpn.logs(stream=True, follow=True):
                    self.log.logger.debug(log.strip().decode())
                    if b"Initialization Sequence Complete" in log:
                        if full:
                            self.log.info("Connected.")
                        else:
                            self.log.info("Connected. Use '--network=container:%s' to attach docker containers.", vpn.name)
