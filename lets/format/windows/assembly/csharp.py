from lets.__module__ import Module
from Cryptodome.Cipher import ARC4
import os, base64, gzip


class CSharp(Module):
    """
    Format a Windows .NET assembly as C# source code.
    """

    @classmethod
    def add_arguments(self, parser):
        parser.add_argument("-c", "--class", dest="cls", type=str, required=True,
            help="class to invoke")
        parser.add_argument("-m", "--method", dest="mth", type=str,
            help="method to invoke")
        parser.add_argument("-a", "--argument", dest="args", type=str, action="append",
            help="argument(s) to provide invocation")
        parser.add_argument("-d", "--debug", action="store_true",
            help="add console logging")

    def handle(self, input, cls=None, mth=None, args=None, debug=False):
        assert input is not None, "Must provide .net assembly as input"

        for data in input:

            # Compress, encrypt, and encode assembly
            key = os.urandom(32)
            compressed = gzip.compress(data, compresslevel=9)
            encrypted = ARC4.new(key).encrypt(compressed)
            encoded = base64.b64encode(encrypted).decode()
            key_encoded = base64.b64encode(key).decode()
            self.log.debug("Packed assembly: %i -> %i bytes", len(data), len(encrypted))
            
            # Unpack assembly
            cmd  =  'DecryptGunzipAndRun(' if mth and mth.lower() != "main" else 'DecryptGunzipAndRunMain('
            cmd +=  '"%s"' % encoded
            cmd +=  ',"%s"' % key_encoded

            # Invoke class
            self.log.debug("Invoked class: %s", cls)
            cmd +=  ',"%s"' % cls

            # Invoke method
            if mth and mth.lower() != "main":
                self.log.debug("Invoked method: %s", mth)
                cmd += ',"%s"' % mth
            else:
                self.log.debug("Invoked method: Main")

            # Add arguments, if specified
            if args:
                for i, arg in enumerate(args):
                    a = None

                    # Add string array argument
                    if arg.startswith("[") and arg.endswith("]"):
                        parts = arg.strip("[]").replace("'", '"').split(",")
                        a = "new string[]{%s}" % (",".join(parts))

                    # Add object array argument
                    elif arg.startswith("{") and arg.endswith("}"):
                        parts = arg.strip("{}").replace("'", '"').split(",")
                        a = "new object[]{%s}" % (",".join(parts))

                    else:
                        # Add integer argument
                        try:
                            a = "%i" % int(arg)

                        # Add string argument
                        except ValueError:
                            a = '"%s"' % arg

                    if a:
                        self.log.debug("Adding argument: %s", a)
                        cmd += ",%s" % a

            cmd +=  ")"

            # Add console logging
            if debug:
                cmd  = "Console.WriteLine(%s);" % cmd
            else:
                cmd += ";"

            # Place command in standard template
            cs = self.template % (self.headers, cmd, self.support)

            yield cs.encode()

    headers = """
using System;
using System.IO;
using System.IO.Compression;
using System.Reflection;
"""

    template = """
%s
class Program
{
    static void Main(string[] args)
    {
        %s
    }
%s
}
"""

    support = """
    private static string _Run(Assembly asm, string cls, string mth, object[] args)
    {
        Type c = asm.GetType(cls);
        if (c == null)
            return "No class: " + cls;

        MethodInfo m;
        object o;

        m = c.GetMethod(mth);
        if (m != null)
        {
            o = Activator.CreateInstance(c, null);
            if (o == null)
                return "Failed to instantiate class: " + cls;
        }
        else
        {
            m = c.GetMethod(mth, BindingFlags.Instance | BindingFlags.NonPublic);
            if (m != null)
            {
                o = Activator.CreateInstance(c, null);
                if (o == null)
                    return "Failed to instantiate class: " + cls;
            }
            else
            {
                m = c.GetMethod(mth, BindingFlags.Static | BindingFlags.NonPublic);
                o = null;
            }
        }

        if (m == null)
            return "No method: " + mth;

        m.Invoke(o, args);
        return null;
    }

    private static string _RunMain(Assembly asm, string cls, string[] args)
    {
        return _Run(asm, cls, "Main", new object[] { args });
    }

    private static byte[] _Gunzip(byte[] bytes)
    {
        byte[] buf = new byte[4096];
        int n;
        using (var msi = new MemoryStream(bytes))
        using (var mso = new MemoryStream())
        {
            using (var gs = new GZipStream(msi, CompressionMode.Decompress))
            {
                while ((n = gs.Read(buf, 0, buf.Length)) != 0)
                    mso.Write(buf, 0, n);
            }
            return mso.ToArray();
        }
    }

    private static byte[] _Decrypt(byte[] bytes, byte[] key)
    {
        byte[] z = new byte[bytes.Length];
        byte[] s = new byte[256];
        byte[] k = new byte[256];
        byte temp;
        int i, j;

        for (i = 0; i < 256; i++)
        {
            s[i] = (byte)i;
            k[i] = key[i % key.GetLength(0)];
        }

        j = 0;
        for (i = 0; i < 256; i++)
        {
            j = (j + s[i] + k[i]) % 256;
            temp = s[i];
            s[i] = s[j];
            s[j] = temp;
        }

        i = j = 0;
        for (int x = 0; x < z.GetLength(0); x++)
        {
            i = (i + 1) % 256;
            j = (j + s[i]) % 256;
            temp = s[i];
            s[i] = s[j];
            s[j] = temp;
            int t = (s[i] + s[j]) % 256;
            z[x] = (byte)(bytes[x] ^ s[t]);
        }
        return z;
    }

    private static string DecryptGunzipAndRun(string encoded, string key, string cls, string mth, params object[] args)
    {
        try
        {
            return _Run(Assembly.Load(_Gunzip(_Decrypt(Convert.FromBase64String(encoded), Convert.FromBase64String(key)))), cls, mth, args);
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

    private static string DecryptGunzipAndRunMain(string encoded, string key, string cls, params string[] args)
    {
        try
        {
            return _RunMain(Assembly.Load(_Gunzip(_Decrypt(Convert.FromBase64String(encoded), Convert.FromBase64String(key)))), cls, args);
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }
"""
