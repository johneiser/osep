from lets.__module__ import Module
import lets.format.windows.assembly.csharp as CSharp


class MSBuild(Module):
    """
    Format a Windows .NET assembly as C# source code wrapped as
    an MSBuild project file (run.csproj).

    Usage:
    $ C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\msbuild.exe run.csproj
    """
    add_arguments = CSharp.add_arguments

    def handle(self, input, **kwargs):
        assert input is not None, "Must provide .net assembly as input"

        CSharp.headers  = self.headers
        CSharp.template = self.template
        for output in CSharp.handle(input, **kwargs):
            yield output
            self.log.info("Usage: C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\msbuild.exe run.csproj")

    headers = """
using System;
using System.IO;
using System.IO.Compression;
using System.Reflection;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
"""

    template = """<Project ToolsVersion="4.0" xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
 <Target Name="Program">
 <Program />
 </Target>
 <UsingTask
 TaskName="Program"
 TaskFactory="CodeTaskFactory"
 AssemblyFile="C:\\Windows\\Microsoft.Net\\Framework\\v4.0.30319\\Microsoft.Build.Tasks.v4.0.dll" >
 <Task>
 <Code Type="Class" Language="cs">
 <![CDATA[
%s
public class Program : Task, ITask
{
    public override bool Execute()
    {
        %s
        return true;
    }
%s
}
 ]]>
 </Code>
 </Task>
 </UsingTask>
 </Project>
 """
