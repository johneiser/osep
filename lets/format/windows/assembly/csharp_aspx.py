from lets.__module__ import Module
import lets.format.windows.assembly.csharp as CSharp


class CSharp_ASPX(Module):
    """
    Format a Windows .NET assembly as C# source code, compatible
    with the ASPX format.

    Dependencies:
    - System
    - System.IO
    - System.IO.Compression
    - System.Reflection
    """

    add_arguments = CSharp.add_arguments

    def handle(self, input, **kwargs):
        assert input is not None, "Must provide .net assembly as input"

        CSharp.headers  = self.headers
        CSharp.template = self.template
        for output in CSharp.handle(input, **kwargs):
            yield output

    headers = """
using System;
using System.IO;
using System.IO.Compression;
using System.Reflection;
"""

    template = """
%s
protected void Page_Load(object sender, EventArgs e)
{
    %s
}
%s
"""
