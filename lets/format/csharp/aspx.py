from lets.__module__ import Module


class ASPX(Module):
    """
    Format C# source code as a .NET ASPX file.

    Note: Code needs to implement the following:

        protected void Page_Load(object sender, EventArgs e)
    """

    @classmethod
    def add_arguments(cls, parser):
        pass

    def handle(self, input):
        assert input is not None, "Must provide csharp as input"

        for data in input:
            source = data.decode()
            lines = source.splitlines()

            # Discover namespace dependencies
            headers = []
            for line in [l for l in lines]:
                if line.startswith("using"):
                    _,_,dependency = line.strip(";").partition(" ")
                    self.log.debug("Identified dependency: %s", dependency)

                    # Replace traditional imports with aspx imports
                    headers.append("<%%@ Import Namespace=\"%s\" %%>" % dependency)
                    lines.remove(line)

            yield (self.template % ("\n".join(headers), "\n".join(lines))).encode()
        
    template = """<%%@ Page Language="C#" %%>
%s
<script runat="server">
%s
</script>
"""
