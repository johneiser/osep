from lets.__module__ import Module
import os


class LNK(Module):
    """
    Convert an arbitrary command into a LNK shortcut.
    """

    def handle(self, input):
        assert input is not None, "Must provide command as input"

        # Fetch LNK template
        base,_,_ = self.__file__.partition(self.__name__.replace(os.path.extsep, os.path.sep))
        path = os.path.join(base, "lets", "__data__", "template.lnk")
        with open(path, "rb") as f:
            template = f.read()

        b = 0x240
        e = 0x408
        m = (e-b)

        for data in input:
            data = data.strip().decode().encode("utf-16-be")
            assert len(data) < m, "Command cannot be larger than %i (%i)" % (m, len(data))
            yield template[:b] + data + template[b+len(data):]
